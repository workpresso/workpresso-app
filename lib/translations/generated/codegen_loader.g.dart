// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "language": "US - English",
  "loading": "please wait a moment.....",
  "languages": {
    "th": "ภาษาไทย",
    "en": "US - English"
  },
  "languages_switch": {
    "th": "TH",
    "en": "EN"
  },
  "nameapp": "Workpresso",
  "enter": "Enter",
  "sign_out": "Sign out",
  "menu": {
    "home": "Home",
    "coupon": "Coupon",
    "profile": "Profile",
    "notify": "Notify",
    "other": "Other"
  },
  "home": {
    "register": {
      "title": "Sign up",
      "des": "Get special privileges before anyone else"
    },
    "section": {
      "service_title": "Service",
      "promotion_title": "Promotions"
    }
  },
  "other": {
    "contact": {
      "title": "ช่องทางการติดต่อ",
      "line": "Line",
      "facebook": "Facebook",
      "youtube": "Youtube"
    },
    "contact_us": {
      "title": "ติดต่อเรา / แจ้งปัญหาการใช้งาน",
      "email": "Email",
      "call_center": "Call Center",
      "notify": "แจ้งเตือน"
    },
    "legal_requirements": {
      "title": "ข้อกำหนดทางกฏหมาย",
      "condition": "Terms & Condition",
      "policy": "Policy"
    }
  },
  "profile": {
    "point": "POINT"
  },
  "button": {
    "btn_ok": "OK",
    "btn_edit_profile": "Save"
  },
  "success": {
    "suc_signin": "You’re now logged in with "
  },
  "error": {
    "no_sign_in": "You are not a member",
    "no_input_number": "กรุณากรอกเบอร์โทรศัพท์",
    "no_input_number_incorrect": "รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง",
    "no_input_otp": "กรุณาระบุ OTP",
    "no_process": "Unable to perform",
    "no_input_info": "กรุณากรอกข้อมูลให้ครบทุกช่อง",
    "no_email_format": "รูปแบบอีเมล์ไม่ถูกต้อง"
  },
  "sign_in": {
    "title": "ลงชื่อเข้าใช้ระบบ",
    "des": "กรุณากรอกอีเมลและรหัสการงานเพื่อเข้าสู่ระบบ",
    "text_1": "อีเมล",
    "text_2": "รหัสผ่าน",
    "text_3": "ลืมรหัสผ่าน?",
    "text_4": "จำรหัสผ่าน",
    "text_5": "เข้าใช้ระบบ",
    "text_6": "ยังไม่มีบัญชีใช้งาน?",
    "text_7": "สร้างบัญชี",
    "error": {
      "err_1": "คุณยังไม่ได้ใส่อีเมล",
      "err_2": "คุณยังไม่ได้ใส่รหัสผ่าน",
      "err_3": "อีเมล์หรือรหัสผ่านไม่ถูกต้องกรุณาตรวจสอบ",
      "err_4": "ไม่พบอีเมล์ของท่านในระบบ"
    }
  },
  "sign_up": {
    "title": "สร้างบัญชีผู้ใช้งาน",
    "des": "กรุณากรอกรายละเอียดเพื่อสร้างบัญชี",
    "text_1": "อีเมล",
    "text_2": "รหัสผ่าน",
    "text_3": "ยืนยันรหัสผ่าน",
    "text_4": "จำรหัสผ่าน",
    "text_5": "ยอมรับ",
    "text_5_1": "เงื่อนไขการใช้งาน",
    "text_6": "ลงทะเบียน",
    "text_7": "มีบัญชีใช้งานแล้ว?",
    "text_8": "เข้าใช้ระบบ",
    "text_9": "ชื่อ",
    "text_10": "นามสกุล",
    "text_11": "กรุณายืนยันอีเมล",
    "text_12": "ระบบได้ทำการส่งลิงก์เพื่อยืนยันตัวตน กรุณาคลิกลิงก์ในอีเมล์เพื่อยืนยันตัวตนของคุณ",
    "text_13": "ยืนยันตัวตนแล้ว?",
    "text_14": "เข้าสู่ระบบ",
    "termsofuse": {
      "text_1": "เงื่อนไขในการใช้งาน"
    },
    "error": {
      "err_1": "คุณยังไม่ได้ใส่อีเมล",
      "err_2": "คุณยังไม่ได้ใส่รหัสผ่าน",
      "err_3": "รหัสผ่านไม่ตรงกันกรุณาตรวจสอบ",
      "err_4": "คุณยังไม่ได้ใส่ยืนยันรหัสผ่าน",
      "err_5": "คุณยังไม่ได้ใส่ชื่อ",
      "err_6": "คุณยังไม่ได้ใส่นามสกุล",
      "err_7": "เมล์นี้ถูกใช้งานแล้ว",
      "err_8": "ไม่สามารถทำรายการได้",
      "err_9": "รหัสผ่านต้องมากกว่า 8 อักษร"
    }
  },
  "forgot": {
    "title": "ตั้งรหัสผ่านใหม่",
    "des": "ระบุอีเมลที่ใช้ในการสมัคร ระบบจะทำการ\nส่งลิงก์สำหรับการตั้งค่ารหัสใหม่ไปทางอีเมลดังกล่าว",
    "sent": {
      "title": "อีเมลถูกจัดส่งแล้ว!",
      "text_1": "ระบบได้ทำการส่งลิงก์สำหรับการตั้งค่ารหัสใหม่ไปทาง",
      "text_2": "เรียบร้อยแล้ว",
      "text_3": "กรุณาตรวจสอบอีเมลดังกล่าว",
      "text_4": "กลับไปที่หน้าแรก"
    }
  },
  "register": {
    "step_one": {
      "title": "ลงทะเบียน",
      "text_1": "เบอร์โทรศัพท์",
      "text_2": "ขั้นตอนต่อไป >",
      "text_3": "By cllick continue, your agree to our",
      "text_4": " Terms of Use ",
      "text_5": "and acknowledge that have read our",
      "text_6": " Privacy Policy "
    },
    "step_two": {
      "title": "Enter the 4-digit code sent to",
      "text_1": "ส่งอีกครั้ง",
      "text_2": "ขั้นตอนต่อไป >",
      "text_3": "ส่งได้อีกครั้งใน "
    },
    "step_tree": {
      "title": "ข้อมูลส่วนตัว",
      "text_1": "ชื่อ",
      "text_2": "นามสกุล",
      "text_3": "อีเมล์",
      "text_5": "วันเกิด",
      "text_4": "ขั้นตอนต่อไป >",
      "text_6": "กรุณาเลือกวันเกิด"
    },
    "step_four": {
      "title": "Term & condition",
      "text_1": "Policy & Term",
      "text_2": "ลงทะเบียน"
    },
    "step_alert": {
      "text_1": "You’re now logged in with "
    }
  },
  "test": "ทดสอบ",
  "tutorial": {
    "text_1": "ถัดไป",
    "text_2": "ข้าม",
    "text_3": "เริ่มใช้งาน",
    "slide_1": {
      "title": "",
      "dec": "When you need to innovate,\nyou need collaboration."
    },
    "slide_2": {
      "title": "",
      "dec": "Talent wins games,\nbut teamwork and intelligence\nwins championships."
    },
    "slide_3": {
      "title": "เพิ่มแรงดึงดูดให้ยอดปัง",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    },
    "slide_4": {
      "title": "จะเสียเวลาค้นหาไปทำไม",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    },
    "slide_5": {
      "title": "เป้าหมายมีไว้ให้พุ่งชน",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    }
  }
};
static const Map<String,dynamic> th = {
  "language": "ภาษาไทย",
  "loading": "กรุณารอสักครู่.....",
  "languages": {
    "th": "ภาษาไทย",
    "en": "US - English"
  },
  "languages_switch": {
    "th": "TH",
    "en": "EN"
  },
  "nameapp": "Workpresso",
  "enter": "เข้าใช้งาน",
  "sign_out": "ล็อกเอาท์",
  "menu": {
    "home": "หน้าหลัก",
    "coupon": "คูปอง",
    "profile": "โปรไฟล์",
    "notify": "แจ้งเตือน",
    "other": "อื่นๆ"
  },
  "home": {
    "register": {
      "title": "ลงทะเบียนใช้งาน",
      "des": "รับสิทธิ์พิเศษก่อนใคร"
    },
    "section": {
      "service_title": "บริการ",
      "promotion_title": "โปรโมชั่น"
    }
  },
  "other": {
    "contact": {
      "title": "ช่องทางการติดต่อ",
      "line": "Line",
      "facebook": "Facebook",
      "youtube": "Youtube"
    },
    "contact_us": {
      "title": "ติดต่อเรา / แจ้งปัญหาการใช้งาน",
      "email": "Email",
      "call_center": "Call Center",
      "notify": "แจ้งเตือน"
    },
    "legal_requirements": {
      "title": "ข้อกำหนดทางกฏหมาย",
      "condition": "Terms & Condition",
      "policy": "Policy"
    }
  },
  "profile": {
    "point": "คะแนน"
  },
  "button": {
    "btn_ok": "ตกลง",
    "btn_edit_profile": "บันทึก"
  },
  "success": {
    "suc_signin": "You’re now logged in with "
  },
  "error": {
    "no_sign_in": "คุณยังไม่เป็นสมาชิก",
    "no_input_number": "กรุณากรอกเบอร์โทรศัพท์",
    "no_input_number_incorrect": "รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง",
    "no_input_otp": "กรุณาระบุ OTP",
    "no_process": "ไม่สามารถดำเนินการได้",
    "no_input_info": "กรุณากรอกข้อมูลให้ครบทุกช่อง",
    "no_email_format": "รูปแบบอีเมล์ไม่ถูกต้อง"
  },
  "sign_in": {
    "title": "ลงชื่อเข้าใช้ระบบ",
    "des": "กรุณากรอกอีเมลและรหัสการงานเพื่อเข้าสู่ระบบ",
    "text_1": "อีเมล",
    "text_2": "รหัสผ่าน",
    "text_3": "ลืมรหัสผ่าน?",
    "text_4": "จำรหัสผ่าน",
    "text_5": "เข้าใช้ระบบ",
    "text_6": "ยังไม่มีบัญชีใช้งาน?",
    "text_7": "สร้างบัญชี",
    "error": {
      "err_1": "คุณยังไม่ได้ใส่อีเมล",
      "err_2": "คุณยังไม่ได้ใส่รหัสผ่าน",
      "err_3": "อีเมล์หรือรหัสผ่านไม่ถูกต้องกรุณาตรวจสอบ",
      "err_4": "ไม่พบอีเมล์ของท่านในระบบ"
    }
  },
  "sign_up": {
    "title": "สร้างบัญชีผู้ใช้งาน",
    "des": "กรุณากรอกรายละเอียดเพื่อสร้างบัญชี",
    "text_1": "อีเมล",
    "text_2": "รหัสผ่าน",
    "text_3": "ยืนยันรหัสผ่าน",
    "text_4": "จำรหัสผ่าน",
    "text_5": "ยอมรับ",
    "text_5_1": "เงื่อนไขการใช้งาน",
    "text_6": "ลงทะเบียน",
    "text_7": "มีบัญชีใช้งานแล้ว?",
    "text_8": "เข้าใช้ระบบ",
    "text_9": "ชื่อ",
    "text_10": "นามสกุล",
    "text_11": "กรุณายืนยันอีเมล",
    "text_12": "ระบบได้ทำการส่งลิงก์เพื่อยืนยันตัวตน กรุณาคลิกลิงก์ในอีเมล์เพื่อยืนยันตัวตนของคุณ",
    "text_13": "ยืนยันตัวตนแล้ว?",
    "text_14": "เข้าสู่ระบบ",
    "termsofuse": {
      "text_1": "เงื่อนไขในการใช้งาน"
    },
    "error": {
      "err_1": "คุณยังไม่ได้ใส่อีเมล",
      "err_2": "คุณยังไม่ได้ใส่รหัสผ่าน",
      "err_3": "รหัสผ่านไม่ตรงกันกรุณาตรวจสอบ",
      "err_4": "คุณยังไม่ได้ใส่ยืนยันรหัสผ่าน",
      "err_5": "คุณยังไม่ได้ใส่ชื่อ",
      "err_6": "คุณยังไม่ได้ใส่นามสกุล",
      "err_7": "เมล์นี้ถูกใช้งานแล้ว",
      "err_8": "ไม่สามารถทำรายการได้",
      "err_9": "รหัสผ่านต้องมากกว่า 8 อักษร"
    }
  },
  "forgot": {
    "title": "ตั้งรหัสผ่านใหม่",
    "des": "ระบุอีเมลที่ใช้ในการสมัคร ระบบจะทำการ\nส่งลิงก์สำหรับการตั้งค่ารหัสใหม่ไปทางอีเมลดังกล่าว",
    "sent": {
      "title": "อีเมลถูกจัดส่งแล้ว!",
      "text_1": "ระบบได้ทำการส่งลิงก์สำหรับการตั้งค่ารหัสใหม่ไปทาง",
      "text_2": "เรียบร้อยแล้ว",
      "text_3": "กรุณาตรวจสอบอีเมลดังกล่าว",
      "text_4": "กลับไปที่หน้าแรก"
    }
  },
  "register": {
    "step_one": {
      "title": "ลงทะเบียน",
      "text_1": "เบอร์โทรศัพท์",
      "text_2": "ขั้นตอนต่อไป >",
      "text_3": "By cllick continue, your agree to our",
      "text_4": " Terms of Use ",
      "text_5": "and acknowledge that have read our",
      "text_6": " Privacy Policy "
    },
    "step_two": {
      "title": "Enter the 4-digit code sent to",
      "text_1": "ส่งอีกครั้ง",
      "text_2": "ขั้นตอนต่อไป >",
      "text_3": "ส่งได้อีกครั้งใน "
    },
    "step_tree": {
      "title": "ข้อมูลส่วนตัว",
      "text_1": "ชื่อ",
      "text_2": "นามสกุล",
      "text_3": "อีเมล์",
      "text_5": "วันเกิด",
      "text_4": "ขั้นตอนต่อไป >",
      "text_6": "กรุณาเลือกวันเกิด"
    },
    "step_four": {
      "title": "Term & condition",
      "text_1": "Policy & Term",
      "text_2": "ลงทะเบียน"
    },
    "step_alert": {
      "text_1": "You’re now logged in with "
    }
  },
  "test": "ทดสอบ",
  "tutorial": {
    "text_1": "ถัดไป",
    "text_2": "ข้าม",
    "text_3": "เริ่มใช้งาน",
    "slide_1": {
      "title": "",
      "dec": "When you need to innovate,\nyou need collaboration."
    },
    "slide_2": {
      "title": "",
      "dec": "Talent wins games,\nbut teamwork and intelligence\nwins championships."
    },
    "slide_3": {
      "title": "เพิ่มแรงดึงดูดให้ยอดปัง",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    },
    "slide_4": {
      "title": "จะเสียเวลาค้นหาไปทำไม",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    },
    "slide_5": {
      "title": "เป้าหมายมีไว้ให้พุ่งชน",
      "dec": "ด้วยระบบบริหารที่จะเชื่อมต่อผู้ซื้อและผู้ขายให้แมทซ์กัน"
    }
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "th": th};
}
