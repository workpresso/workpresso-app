import 'package:auto_route/auto_route.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_font.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/ui/route/router.gr.dart';

class TutorialPage extends StatefulWidget {
  const TutorialPage({Key? key}) : super(key: key);

  @override
  _TutorialPageState createState() => _TutorialPageState();
}

class _TutorialPageState extends State<TutorialPage> {
  final CarouselController _controller = CarouselController();
  var _current = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primary_3,
      appBar: BaseAppBar(
        backgroupColor: primary_3,
        isShowAppBar: true,
        centerTitle: true,
        leftStatusBaseAppBar: LeftStatusBaseAppBar.none,
        onLeftPressCallback: () {
          Navigator.pop(context);
        },
        centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
        rightStatusBaseAppBar: RightStatusBaseAppBar.none,
      ),
      body: SafeArea(
          child: Column(
        children: [
          _getContent(),
        ],
      )),
    );
  }

  Widget _getContent() {
    List<Widget>? imagesList = [
      _tutorialPresent(AppAssets.tutorial_1, 1),
      _tutorialPresent(AppAssets.tutorial_2, 2),
      _tutorialPresent(AppAssets.tutorial_3, 3),
      _tutorialPresent(AppAssets.tutorial_4, 4),
      _tutorialPresent(AppAssets.tutorial_5, 5),
    ];
    String titleBTN = tr('tutorial.text_1');
    String iconBTN = AppAssets.arrow_rigth_while;
    bool _isEnableIcon = true;
    if (_current == imagesList.length - 1) {
      titleBTN = tr('tutorial.text_3');
      _isEnableIcon = false;
    } else {
      titleBTN = tr('tutorial.text_1');
      _isEnableIcon = true;
    }
    return Column(
      children: [
        Container(
          height: 50.h,
          child: CarouselSlider(
            items: imagesList,
            carouselController: _controller,
            options: CarouselOptions(
              height: 236,
              initialPage: 0,
              aspectRatio: 6 / 5,
              viewportFraction: 1.1,
              enableInfiniteScroll: false,
              autoPlay: false,
              autoPlayInterval: Duration(seconds: 5),
              autoPlayAnimationDuration: Duration(milliseconds: 1000),
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 60, top: 20),
          width: 90.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imagesList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 15.0,
                  height: 15.0,
                  margin: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 4.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == entry.key
                        ? primary_5.withOpacity(0.5)
                        : primary_5,
                    border: Border.all(
                      color: Colors.white.withOpacity(1),
                      width: 2.0,
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
        GestureDetector(
          onTap: () async {
            if (_current < imagesList.length - 1) {
              _controller.nextPage();
            } else if (_current == imagesList.length - 1) {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setBool(firstInstall, true);
              context.router.push(SignInRoute());
            }
          },
          behavior: HitTestBehavior.opaque,
          child: Container(
            width: 30.w,
            padding: EdgeInsets.only(left: 5, right: 5, bottom: 5),
            decoration: BoxDecoration(
              color: primary_10,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      titleBTN,
                      style: BaseTextStyle().custom(
                        color: primary_5,
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _isEnableIcon,
                  child: Container(
                    margin: EdgeInsets.only(top: 8, left: 5),
                    width: 17,
                    height: 17,
                    child: Image.asset(iconBTN),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _tutorialPresent(String partImg, int index) {
    String title;
    String des;
    title = tr('tutorial.slide_${index}.title');
    des = tr('tutorial.slide_${index}.dec');
    return Container(
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 15),
            child: Image.asset(
              partImg,
              width: 275,
            ),
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: BaseTextStyle().custom(
              color: primary_5,
              fontSize: 24,
              fontWeight: FontWeight.normal,
            ),
          ),
          Text(
            des,
            textAlign: TextAlign.center,
            style: BaseTextStyle().custom(
              color: primary_5,
              fontSize: 15,
              fontWeight: FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }
}
