import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';

class CouponPage extends StatefulWidget {
  const CouponPage({Key? key}) : super(key: key);

  @override
  _CouponPageState createState() => _CouponPageState();
}

class _CouponPageState extends State<CouponPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowAppBar: true,
        centerTitle: true,
        centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
        titleBaseAppBar: tr('menu.coupon'),
        rightStatusBaseAppBar: RightStatusBaseAppBar.profile,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 25),
          width: 100.w,
          child: Align(
            alignment: Alignment.topCenter,
            child: Text(
              "coming soon....",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
    );
  }
}
