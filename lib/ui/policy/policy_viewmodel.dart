import 'package:flutter/foundation.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/term_policy/policy.dart';
import 'package:workpresso/service/term_policy_service.dart';
import 'package:injectable/injectable.dart';

@injectable
class PolicyViewModel extends ChangeNotifier {
  Future<Resource<PolicyData?>?> getPolicy() async {
    Resource<PolicyData?>? result;
    result = await getIt<TermPolicyService>().getPolicy();
    return result;
  }
}
