import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import 'policy_viewmodel.dart';

class PolicyPage extends StatefulWidget {
  const PolicyPage({Key? key}) : super(key: key);

  @override
  _PolicyPageState createState() => _PolicyPageState();
}

class _PolicyPageState extends State<PolicyPage> {
  String? title = tr('other.legal_requirements.policy');
  String? detail = "";
  bool reload = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    context.read<PolicyViewModel>().getPolicy().then((value) {
      if (value?.data?.title != null) {
        title = value?.data?.title ?? "";
      }
      if (value?.data?.detail != null) {
        detail = value?.data?.detail ?? "";
      }
      if (reload) {
        setState(() {
          reload = false;
        });
      }
    });

    return Scaffold(
      appBar: BaseAppBar(
        isShowAppBar: true,
        centerTitle: true,
        leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
        onLeftPressCallback: () {
          Navigator.pop(context);
        },
        centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
        titleBaseAppBar: title ?? "",
        rightStatusBaseAppBar: RightStatusBaseAppBar.none,
      ),
      body: SafeArea(
          child: Column(
        children: [
          _getContent(),
        ],
      )),
    );
  }

  Widget _getContent() {
    return Container(
      height: 82.h,
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(top: 26, left: 27, right: 30),
            child: Html(
              data: detail ?? "",
            ),
          ),
        ),
      ),
    );
  }
}
