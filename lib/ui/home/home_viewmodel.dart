import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/banner.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/home_service.dart';

@injectable
class HomeViewModel extends ChangeNotifier {
  int? selectMenu;
  List<String> bannerTop = [];
  List<String> bannerSlide = [];
  List<String> bannerPromotion = [];
  List<Bannerlists?>? bannerIcon = [];

  Future<void> setSelectMenuBar(int value) async {
    selectMenu = value;
    notifyListeners();
  }

  Future<Resource<Banner?>?> getBannerTop() async {
    Resource<Banner?>? result;
    result = await getIt<HomeService>().getBannerTop();
    if (result?.data?.banners != null) {
      bannerTop.clear();
      result?.data?.banners?.forEach((element) {
        if (element?.thumbnail?.cover != null) {
          if (element?.thumbnail?.cover != "") {
            bannerTop.add("${element?.thumbnail?.cover ?? ""}");
          }
        }
      });
    }
    return result;
  }

  Future<Resource<Banner?>?> getBannerSlide() async {
    Resource<Banner?>? result;
    result = await getIt<HomeService>().getBannerSlide();
    if (result?.data?.banners != null) {
      bannerSlide.clear();
      result?.data?.banners?.forEach((element) {
        if (element?.thumbnail?.cover != null) {
          if (element?.thumbnail?.cover != "") {
            bannerSlide.add("${element?.thumbnail?.cover ?? ""}");
          }
        }
      });
    }
    return result;
  }

  Future<Resource<Banner?>?> getBannerPromotion() async {
    Resource<Banner?>? result;
    result = await getIt<HomeService>().getBannerPromotion();
    if (result?.data?.banners != null) {
      bannerPromotion.clear();
      result?.data?.banners?.forEach((element) {
        if (element?.thumbnail?.cover != null) {
          if (element?.thumbnail?.cover != "") {
            bannerPromotion.add("${element?.thumbnail?.cover ?? ""}");
          }
        }
      });
    }
    return result;
  }

  Future<Resource<Banner?>?> getBannerIcon() async {
    Resource<Banner?>? result;
    result = await getIt<HomeService>().getBannerIcon();
    if (result?.data?.banners != null) {
      bannerIcon = result?.data?.banners;
    }
    return result;
  }

  Future<void> clearTokenCustome() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(tokenCustomer);
    prefs.remove(refreshToken);
  }
}
