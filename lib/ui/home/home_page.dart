import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_font.dart';
import 'package:workpresso/models/banner.dart';
import 'package:workpresso/ui/coupon/coupon_page.dart';
import 'package:workpresso/ui/home/home_viewmodel.dart';
import 'package:workpresso/ui/notify/notify_page.dart';
import 'package:workpresso/ui/other/other_page.dart';
import 'package:workpresso/ui/profile/profile_page.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/registers/step_four/step_four_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/ui/signIn/sign_in_viewmodel.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isShow = false;
  PersistentTabController? _controllerMenu;

  @override
  void initState() {
    super.initState();
    _controllerMenu = PersistentTabController(initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    context.watch<ProfileViewModel>().checkSignIn();
    isShow = context.read<ProfileViewModel>().isSignInLocal;
    var regiscomplete =
        Provider.of<ProfileViewModel>(context, listen: true).regisComplete;
    if (regiscomplete) {
      context.read<ProfileViewModel>().regisComplete = false;
      Future.delayed(const Duration(milliseconds: 700), () {
        _showRegisterSuccess(context);
      });
    }
    var issignout =
        Provider.of<ProfileViewModel>(context, listen: true).isSignOut;
    if (issignout) {
      context.read<ProfileViewModel>().isSignOut = false;
      _controllerMenu?.jumpToTab(0);
    }

    int? selectedMenu =
        Provider.of<HomeViewModel>(context, listen: true).selectMenu;
    if (issignout != null) {
      context.read<HomeViewModel>().selectMenu = null;
      if (selectedMenu != null) {
        if (selectedMenu == 2) {
          context.read<ProfileViewModel>().getCustomerProfile();
          _controllerMenu?.jumpToTab(selectedMenu);
        } else {
          _controllerMenu?.jumpToTab(selectedMenu);
        }
      }
    }

    return _buildContent();
  }

  Widget _buildContent() {
    // return _buildNavigateBottomSection();
    return SafeArea(
        child: Container(
      child: Column(children: [
        Text("${context.read<SignInViewModel>().firstName}"),
        Text("${context.read<SignInViewModel>().lastName}"),
        GestureDetector(
          onTap: () {
            context.read<HomeViewModel>().clearTokenCustome();
            Future.delayed(const Duration(milliseconds: 100), () {
              context.router.push(SignInRoute());
            });
          },
          child: Container(
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.only(top: 5, bottom: 5),
            width: 150,
            alignment: Alignment.center,
            color: primary_3,
            child: Text(
              'Logout',
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ]),
    ));
  }

  //pull to refresh
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  void _onRefresh() async {
    // monitor network fetch
    context.read<HomeViewModel>().getBannerTop();
    context.read<HomeViewModel>().getBannerSlide();
    context.read<HomeViewModel>().getBannerPromotion();
    context.read<HomeViewModel>().getBannerIcon();
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    setState(() {});
    _refreshController.refreshCompleted();
  }

  Widget homeScreen() {
    return SmartRefresher(
      controller: _refreshController,
      enablePullDown: true,
      enablePullUp: false,
      onRefresh: _onRefresh,
      header: MaterialClassicHeader(
        color: primary_3,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildBannerTopSection(),
            _buildServiceSection(),
            _buildBannerSlide(),
            _buildBannerPromotionSection(),
          ],
        ),
      ),
    );
  }

  Widget _buildNavigateBottomSection() {
    List<Widget> _buildScreens() {
      return [
        homeScreen(),
        CouponPage(),
        ProfilePage(),
        NotifyPage(),
        OtherPage(),
      ];
    }

    List<PersistentBottomNavBarItem> _navBarsItems() {
      return [
        PersistentBottomNavBarItem(
          icon: SvgPicture.asset(
            AppAssets.menuHome,
            color: primary_3,
          ),
          inactiveIcon: SvgPicture.asset(
            AppAssets.menuHome,
            color: primary_4,
          ),
          title: (tr('menu.home')),
          activeColorPrimary: primary_3,
          inactiveColorPrimary: primary_4,
        ),
        PersistentBottomNavBarItem(
          icon: SvgPicture.asset(
            AppAssets.menuCouponCS,
            // color: primary_3,
          ),
          inactiveIcon: SvgPicture.asset(
            AppAssets.menuCouponCS,
            // color: primary_4,
          ),
          title: (tr('menu.coupon')),
          activeColorPrimary: primary_3,
          inactiveColorPrimary: primary_4,
          onPressed: (result) {},
        ),
        PersistentBottomNavBarItem(
            icon: Image.asset(AppAssets.menuProfile),
            title: (tr('menu.profile')),
            activeColorPrimary: primary_3,
            inactiveColorPrimary: primary_4,
            onPressed: (result) {
              if (context.read<ProfileViewModel>().isSignInLocal) {
                context.read<ProfileViewModel>().getCustomerProfile();
                _controllerMenu?.jumpToTab(2);
              } else {
                if (_controllerMenu?.index == 0) {
                  context.router.push(StepOneRoute());
                } else {
                  _controllerMenu?.jumpToTab(0);
                  Future.delayed(const Duration(milliseconds: 200), () {
                    context.router.push(StepOneRoute());
                  });
                }
                // showDialog(
                //   context: context,
                //   builder: (BuildContext contexgt) => AlertDialog(
                //     // title: Text("Workpresso"),
                //     content: Text(tr('error.no_sign_in')),
                //     actions: <Widget>[
                //       TextButton(
                //           onPressed: () =>
                //               Navigator.pop(context, tr('button.btn_ok')),
                //           child: Text(
                //             tr('button.btn_ok'),
                //             style: TextStyle(color: Colors.black),
                //           ))
                //     ],
                //   ),
                // );
              }
            }),
        PersistentBottomNavBarItem(
          icon: SvgPicture.asset(
            AppAssets.menuNotifyCS,
            // color: primary_3,
          ),
          inactiveIcon: SvgPicture.asset(
            AppAssets.menuNotifyCS,
            // color: primary_4,
          ),
          title: (tr('menu.notify')),
          activeColorPrimary: primary_3,
          inactiveColorPrimary: primary_4,
          onPressed: (result) {},
        ),
        PersistentBottomNavBarItem(
          icon: SvgPicture.asset(
            AppAssets.menuOther,
            color: primary_3,
          ),
          inactiveIcon: SvgPicture.asset(
            AppAssets.menuOther,
            color: primary_4,
          ),
          title: (tr('menu.other')),
          activeColorPrimary: primary_3,
          inactiveColorPrimary: primary_4,
        ),
      ];
    }

    return Container(
      width: 100.w,
      height: 100,
      child: PersistentTabView(
        context,
        controller: _controllerMenu,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.white, // Default is Colors.white.
        handleAndroidBackButtonPress: true, // Default is true.
        resizeToAvoidBottomInset:
            true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
        stateManagement: true, // Default is true.
        hideNavigationBarWhenKeyboardShows:
            true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
        decoration: NavBarDecoration(
          borderRadius: BorderRadius.circular(10.0),
          colorBehindNavBar: Colors.white,
        ),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: ItemAnimationProperties(
          // Navigation Bar's items animation properties.
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: ScreenTransitionAnimation(
          // Screen transition animation on change of selected tab.
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        navBarStyle:
            NavBarStyle.style15, // Choose the nav bar style with this property.
      ),
    );
  }

  Widget _buildBannerTopSection() {
    final CarouselController _controller = CarouselController();
    final List<Widget> imageSliders = context
        .watch<HomeViewModel>()
        .bannerTop
        .map((item) => Container(
              child: Container(
                margin: EdgeInsets.all(0.0),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          fit: BoxFit.fill,
                          width: 1000.0,
                          height: 236,
                          cacheKey: '${item}',
                          imageUrl: '${item}',
                          errorWidget: (context, url, error) => Image.asset(
                            AppAssets.imagePlaceHolde,
                            width: 1000.0,
                            height: 236,
                          ),
                        ),
                      ],
                    )),
              ),
            ))
        .toList();
    return Container(
      width: 100.w,
      height: isShow ? 250 : 280,
      color: Colors.white,
      child: Stack(
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
              height: 236,
              initialPage: 0,
              aspectRatio: 6 / 5,
              viewportFraction: 1.1,
              enableInfiniteScroll: false,
              autoPlay: false,
              autoPlayInterval: const Duration(seconds: 5),
              autoPlayAnimationDuration: const Duration(milliseconds: 800),
              scrollPhysics: const NeverScrollableScrollPhysics(),
            ),
          ),
          Visibility(
            visible: false,
            child: Positioned(
              width: 100.w,
              bottom: isShow ? 20 : 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imageSliders.asMap().entries.map((entry) {
                  return GestureDetector(
                    onTap: () => _controller.animateToPage(entry.key),
                    child: Container(
                      width: 10.0,
                      height: 10.0,
                      margin: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 4.0),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: primary_3),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          _buildMemberTopRight(),
          _buildRegisterSection(),
        ],
      ),
    );
  }

  Widget _buildMemberTopRight() {
    return Visibility(
      visible: isShow,
      child: Positioned(
        top: 0,
        right: 16,
        bottom: isShow ? 150 : 170,
        child: GestureDetector(
          onTap: () {
            context.read<ProfileViewModel>().getCustomerProfile();
            _controllerMenu?.jumpToTab(2);
          },
          child: Container(
            child: SvgPicture.asset(
              AppAssets.icon_member,
              width: 25,
              height: 25,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildRegisterSection() {
    return Visibility(
      visible: isShow == false ? true : false,
      child: Positioned(
        width: 100.w,
        top: 210,
        child: Align(
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () {
              //Mew
              context.router.push(StepOneRoute());
            },
            child: Container(
              width: 62.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                    color: indicatorOpa,
                    spreadRadius: 0,
                    blurRadius: 4,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Image.asset(
                    AppAssets.logo,
                    width: 66,
                    height: 66,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 40.w,
                        child: Text(
                          tr('home.register.title'),
                          maxLines: 1,
                          style: BaseTextStyle().custom(
                            color: primary_3,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: 40.w,
                        child: Text(
                          tr('home.register.des'),
                          maxLines: 1,
                          style: BaseTextStyle().custom(
                            color: textdes,
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  SvgPicture.asset(
                    AppAssets.icon_next,
                    width: 10,
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildServiceSection() {
    bool isShow = false;
    if (context.watch<HomeViewModel>().bannerIcon != null) {
      if (context.watch<HomeViewModel>().bannerIcon!.isNotEmpty) {
        isShow = true;
      }
    }
    return Visibility(
      visible: isShow,
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(26, 0, 0, 5),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  child: Text(
                    tr('home.section.service_title'),
                    style: BaseTextStyle().custom(
                      color: textdes,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 5),
                height: 75,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: context.read<HomeViewModel>().bannerIcon?.length,
                  itemBuilder: (context, index) {
                    return _itemservice(
                        context.read<HomeViewModel>().bannerIcon?[index]);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _itemservice(Bannerlists? bannerData) {
    return Container(
      child: Column(children: [
        CachedNetworkImage(
          width: 88,
          height: 44,
          cacheKey: '${bannerData?.thumbnail?.thumbnail}',
          imageUrl: '${bannerData?.thumbnail?.thumbnail}',
          errorWidget: (context, url, error) => Image.asset(
            AppAssets.menuProfile,
            width: 88,
            height: 44,
          ),
        ),
        Text(bannerData?.title ?? ""),
      ]),
    );
  }

  Future _showRegisterSuccess(BuildContext context) {
    return showModalBottomSheet(
        isDismissible: false,
        isScrollControlled: false,
        context: context,
        builder: (BuildContext context) {
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              setState(() {
                isShow = context.read<ProfileViewModel>().isSignInLocal;
                Navigator.pop(context);
              });
            },
            child: SizedBox(
              width: 100.w,
              height: 220,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10, right: 5),
                    child: Row(children: [
                      Spacer(),
                      Icon(
                        Icons.close,
                        color: primary_3,
                      ),
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0),
                    child: Image.asset(
                      AppAssets.icon_success,
                      width: 66,
                      height: 66,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 26, bottom: 57),
                    child: Text(
                      "${tr('success.suc_signin')} ${context.read<StepFourPageViewModel>().phoneNumberSuccess ?? ""}",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget _buildBannerPromotionSection() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.fromLTRB(26, 0, 0, 5),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                child: Text(
                  tr('home.section.promotion_title'),
                  style: BaseTextStyle().custom(
                    color: textdes,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                itemCount:
                    context.watch<HomeViewModel>().bannerPromotion.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 13, 5),
                    child: GestureDetector(
                      onTap: () {
                        print("Click banner");
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          width: 350,
                          height: 147,
                          cacheKey:
                              '${context.watch<HomeViewModel>().bannerPromotion[index]}',
                          imageUrl:
                              '${context.watch<HomeViewModel>().bannerPromotion[index]}',
                          errorWidget: (context, url, error) => Image.asset(
                            AppAssets.imagePlaceHolde,
                            width: 350,
                            height: 147,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBannerSlide() {
    final CarouselController _controller = CarouselController();
    final List<Widget> imageSliders = context
        .watch<HomeViewModel>()
        .bannerSlide
        .map((item) => Container(
              child: Container(
                margin: EdgeInsets.all(0.0),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          fit: BoxFit.fill,
                          width: 1000.0,
                          height: 170,
                          cacheKey: '${item}',
                          imageUrl: '${item}',
                          errorWidget: (context, url, error) => Image.asset(
                            AppAssets.imagePlaceHolde,
                            width: 1000.0,
                            height: 170,
                          ),
                        ),
                      ],
                    )),
              ),
            ))
        .toList();
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: 100.w,
      height: 170,
      color: Colors.white,
      child: Stack(
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
              height: 236,
              initialPage: 0,
              aspectRatio: 6 / 5,
              viewportFraction: 1.1,
              enableInfiniteScroll: true,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 5),
              autoPlayAnimationDuration: Duration(milliseconds: 1000),
            ),
          ),
          Positioned(
            width: 100.w,
            bottom: 5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: imageSliders.asMap().entries.map((entry) {
                return GestureDetector(
                  onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: 10.0,
                    height: 10.0,
                    margin: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 4.0),
                    decoration:
                        BoxDecoration(shape: BoxShape.circle, color: primary_3),
                  ),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
