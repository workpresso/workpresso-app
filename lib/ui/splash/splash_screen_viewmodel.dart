import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/splash/reference.dart';
import 'package:workpresso/models/splash/splash.dart';
import 'package:workpresso/service/splash_service.dart';

@injectable
class SplashScreenViewModel extends ChangeNotifier {
  Resource<Splash?>? _splashconfig;
  Resource<Splash?>? get splashconfig => _splashconfig;

  Future<Resource<Splash?>?> getSplashConfig() async {
    var result = await getIt<SplashService>().getSplashConfig();
    _splashconfig = result;
    notifyListeners();
  }

  Future<Resource<Splash?>?> getSplashConfigA() async {
    var result = await getIt<SplashService>().getSplashConfig();
    _splashconfig = result;
    return result;
  }

  Future<Resource<Reference?>?> getReference() async {
    var result = await getIt<SplashService>().getReference();
    final prefs = await SharedPreferences.getInstance();
    if (result?.data?.referenceId != null) {
      if (result?.data?.referenceId != "") {
        prefs.setString(referenceId, result?.data?.referenceId ?? "");
      }
    }
  }
}
