import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/ui/splash/splash_screen_viewmodel.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    context.read<ProfileViewModel>().checkSignIn();
    context.read<SplashScreenViewModel>().getReference();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 900), () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.getBool(firstInstall) == true) {
        if (prefs.getString(tokenCustomer) != null) {
          context.router.push(HomeRoute());
        } else {
          context.router.push(SignInRoute());
        }
      } else {
        //Go to tutorial
        context.router.push(TutorialRoute());
      }
    });

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Container(
          width: 100.w,
          color: primary_1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                AppAssets.logo,
                width: 207,
                height: 86,
              )
            ],
          ),
        ),
      ),
    );
  }
}
