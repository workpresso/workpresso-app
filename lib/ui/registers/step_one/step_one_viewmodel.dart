import 'package:flutter/foundation.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/customer/otp_request.dart';
import 'package:workpresso/models/customer/otp_verify.dart';
import 'package:workpresso/models/customer/register_customer.dart';
import 'package:workpresso/models/customer/verify_signin.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/customer_service.dart';
import 'package:injectable/injectable.dart';

@injectable
class StepOnePageViewModel extends ChangeNotifier {
  Future<Resource<OtpRequest?>?> otpRequest(String? phoneno) async {
    Resource<OtpRequest?>? result;
    result = await getIt<CustomerService>().otpRequest(phoneno);
    return result;
  }

  Future<Resource<OtpVerify?>?> otpVerify(String? token, String? pin) async {
    Resource<OtpVerify?>? result;
    result = await getIt<CustomerService>().otpVerify(token, pin);
    return result;
  }

  Future<Resource<VerifySignin?>?> verifySignin(
      String? phoneno, String? token, String? pin) async {
    Resource<VerifySignin?>? result;
    result = await getIt<CustomerService>().verifySignin(phoneno, token, pin);
    return result;
  }

  Future<Resource<RegisterCustomer?>?> registerCustomer(String? firstname,
      String? lastname, String? phoneNo, String? birthDay) async {
    Resource<RegisterCustomer?>? result;
    result = await getIt<CustomerService>()
        .registerCustomer(firstname, lastname, phoneNo, birthDay);
    return result;
  }
}
