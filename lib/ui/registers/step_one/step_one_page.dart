import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/registers/step_one/step_one_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class StepOnePage extends StatefulWidget {
  const StepOnePage({Key? key}) : super(key: key);

  @override
  _StepOnePageState createState() => _StepOnePageState();
}

class _StepOnePageState extends State<StepOnePage> {
  TextEditingController? controllerInput = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SafeArea(
              child: Column(
            children: [
              _buildTitle(),
              _buildInputNumber(),
              _buildButtonNextStep(),
              _buildTermAndPolicy(),
            ],
          )),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 155, bottom: 30),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Text(
          tr('register.step_one.title'),
          style: TextStyle(
              fontSize: 20, color: primary_3, fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Widget _buildInputNumber() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 12),
                child: const Text(
                  "+66",
                  style: TextStyle(
                      fontSize: 20,
                      color: codeCountry,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 9, right: 12),
                width: 1,
                height: 25,
                color: primary_3,
              ),
              Container(
                width: 65.w,
                // height: 15,
                child: TextField(
                  controller: controllerInput,
                  keyboardType: TextInputType.number,
                  inputFormatters: [LengthLimitingTextInputFormatter(10)],
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: tr('register.step_one.text_1'),
                    hintStyle: const TextStyle(
                        fontSize: 14.0,
                        color: codeCountry,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 9, right: 12),
            width: 100.w,
            height: 1,
            color: primary_3,
          ),
        ],
      ),
    );
  }

  Widget _buildButtonNextStep() {
    return Container(
      width: 100.w,
      margin: EdgeInsets.only(top: 18, bottom: 18),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (controllerInput?.text != null) {
            if (controllerInput!.text.length >= 10) {
              EasyLoading.show(
                status: tr('loading'),
                maskType: EasyLoadingMaskType.black,
              );
              context
                  .read<StepOnePageViewModel>()
                  .otpRequest(controllerInput!.text)
                  .then((value) {
                EasyLoading.dismiss();
                context.router.push(StepTwoRoute(
                    countryCode: "+66",
                    phoneNumber: controllerInput?.text ?? "",
                    otprequestdata: value?.data));
              });
            } else {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
              Future.delayed(const Duration(milliseconds: 500), () {
                showOkAlertDialog(
                    context: context,
                    message: tr('error.no_input_number_incorrect'));
              });
            }
          } else {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            Future.delayed(const Duration(milliseconds: 500), () {
              showOkAlertDialog(
                  context: context, message: tr('error.no_input_number'));
            });
          }
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            width: 60.w,
            padding: EdgeInsets.only(top: 14, bottom: 14),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: primary_3)),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                tr('register.step_one.text_2'),
                style: const TextStyle(
                    fontSize: 16,
                    color: primary_3,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTermAndPolicy() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(left: 45, right: 45),
        width: 100.w,
        child: Wrap(
          alignment: WrapAlignment.center,
          children: [
            Text(
              tr('register.step_one.text_3'),
              style: TextStyle(color: textTerms, fontSize: 12),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const TermRoute());
              },
              child: Text(
                tr('register.step_one.text_4'),
                style: TextStyle(color: primary_3, fontSize: 12),
              ),
            ),
            Text(
              tr('register.step_one.text_5'),
              style: TextStyle(color: textTerms, fontSize: 12),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const PolicyRoute());
              },
              child: Text(
                tr('register.step_one.text_6'),
                style: TextStyle(color: primary_3, fontSize: 12),
              ),
            )
          ],
        ),
      ),
    );
  }
}
