import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/utils/utils.dart';
import 'package:sizer/sizer.dart';

class StepTreePage extends StatefulWidget {
  const StepTreePage({this.countryCode, this.phoneNumber, Key? key})
      : super(key: key);

  final String? countryCode;
  final String? phoneNumber;

  @override
  _StepTreePageState createState() => _StepTreePageState();
}

class _StepTreePageState extends State<StepTreePage> {
  TextEditingController? controllerInputName = TextEditingController();
  TextEditingController? controllerInputLastName = TextEditingController();
  TextEditingController? controllerInputEmail = TextEditingController();
  DateTime dateSelected = DateTime.now();
  String? birthDay;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
            onLeftPressCallback: () {
              Navigator.of(context)
                  .popUntil(ModalRoute.withName("StepOneRoute"));
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SafeArea(
              child: SingleChildScrollView(
            child: Wrap(
              children: [
                _buildTitle(),
                _buildInputInfoName(),
                _buildInp0utInfoLastName(),
                _buildInputInfoEMail(),
                _buildInputBirthdate(),
                _buildButtonNextStep(),
              ],
            ),
          )),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 43, bottom: 30),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Text(
          tr('register.step_tree.title'),
          style: TextStyle(
              fontSize: 20, color: primary_3, fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Widget _buildInputInfoName() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27, top: 13),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 12),
                width: 70.w,
                // height: 15,
                child: TextField(
                  cursorHeight: 25,
                  cursorColor: primary_3,
                  controller: controllerInputName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: tr('register.step_tree.text_1'),
                    hintStyle: const TextStyle(
                        fontSize: 14.0,
                        color: codeCountry,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 9, right: 12),
            width: 100.w,
            height: 1,
            color: primary_3,
          ),
        ],
      ),
    );
  }

  Widget _buildInp0utInfoLastName() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27, top: 13),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 12),
                width: 70.w,
                // height: 15,
                child: TextField(
                  cursorHeight: 25,
                  cursorColor: primary_3,
                  controller: controllerInputLastName,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: tr('register.step_tree.text_2'),
                    hintStyle: const TextStyle(
                        fontSize: 14.0,
                        color: codeCountry,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 9, right: 12),
            width: 100.w,
            height: 1,
            color: primary_3,
          ),
        ],
      ),
    );
  }

  Widget _buildInputInfoEMail() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27, top: 13),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 12),
                width: 70.w,
                // height: 15,
                child: TextField(
                  cursorHeight: 25,
                  cursorColor: primary_3,
                  controller: controllerInputEmail,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: tr('register.step_tree.text_3'),
                    hintStyle: const TextStyle(
                        fontSize: 14.0,
                        color: codeCountry,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 9, right: 12),
            width: 100.w,
            height: 1,
            color: primary_3,
          ),
        ],
      ),
    );
  }

  Widget _buildInputBirthdate() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27, top: 13, bottom: 135),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 35,
                margin: EdgeInsets.only(left: 12, right: 10, top: 15),
                child: Text(
                  tr('register.step_tree.text_5'),
                  style: const TextStyle(
                      fontSize: 14,
                      color: codeCountry,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                width: 65.w,
                padding: EdgeInsets.only(top: 2),
                child: GestureDetector(
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(1900, 1, 1),
                        maxTime: DateTime(DateTime.now().year,
                            DateTime.now().month, DateTime.now().day),
                        onChanged: (date) {}, onConfirm: (date) {
                      setState(() {
                        birthDay = "${date.year}-${date.month}-${date.day}";
                      });
                    }, currentTime: DateTime.now(), locale: LocaleType.th);
                  },
                  child: birthDay == null
                      ? Text("${tr('register.step_tree.text_6')}")
                      : Text("${birthDay}"),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 9, right: 12),
            width: 100.w,
            height: 1,
            color: primary_3,
          ),
        ],
      ),
    );
  }

  void onDateChangeCallback(DateTime birthday) {
    birthDay = "${birthday.year}-${birthday.month}-${birthday.day}";
  }

  Widget _buildButtonNextStep() {
    return Container(
      width: 100.w,
      margin: EdgeInsets.only(top: 18, bottom: 18),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (controllerInputName!.text.isEmpty ||
              controllerInputLastName!.text.isEmpty ||
              controllerInputEmail!.text.isEmpty ||
              birthDay == null) {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            Future.delayed(const Duration(milliseconds: 500), () {
              showOkAlertDialog(
                  context: context, message: tr('error.no_input_info'));
            });
          } else {
            if (Utils().validateEmail(controllerInputEmail!.text) == false) {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
              Future.delayed(const Duration(milliseconds: 500), () {
                showOkAlertDialog(
                    context: context, message: tr('error.no_email_format'));
              });
            } else {
              context.router.push(StepFourRoute(
                  countryCode: widget.countryCode,
                  phoneNumber: widget.phoneNumber,
                  customerName: controllerInputName!.text,
                  customerLastName: controllerInputLastName!.text,
                  customerEmail: controllerInputEmail!.text,
                  customerBD: birthDay));
            }
          }
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            width: 60.w,
            padding: EdgeInsets.only(top: 14, bottom: 14),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: primary_3)),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                tr('register.step_one.text_2'),
                style: const TextStyle(
                    fontSize: 16,
                    color: primary_3,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
