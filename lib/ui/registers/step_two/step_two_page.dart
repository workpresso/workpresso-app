import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/models/customer/otp_request.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/registers/step_four/step_four_viewmodel.dart';
import 'package:workpresso/ui/registers/step_one/step_one_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class StepTwoPage extends StatefulWidget {
  const StepTwoPage(
      {this.countryCode, this.phoneNumber, this.otprequestdata, Key? key})
      : super(key: key);
  final OtpRequest? otprequestdata;
  final String? countryCode;
  final String? phoneNumber;

  @override
  _StepTwoPageState createState() => _StepTwoPageState();
}

class _StepTwoPageState extends State<StepTwoPage> {
  TextEditingController? controllerInput = TextEditingController();
  late CountdownTimerController controller;
  int endTime = DateTime.now().millisecondsSinceEpoch;
  bool waitResendOtp = false;
  final pinController = TextEditingController();
  String? tokenRes;

  @override
  void initState() {
    super.initState();
  }

  void onEnd() {
    waitResendOtp = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (widget.otprequestdata != null) {
      if (widget.otprequestdata?.token != null) {
        tokenRes = widget.otprequestdata?.token;
      }
    }
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SafeArea(
              child: Column(
            children: [
              _buildTitle(),
              _buildDisplayPhone(),
              _buildInputOTP(),
              _buildOTPActions(),
              _buildButtonNextStep(),
            ],
          )),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 22, bottom: 2),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Text(
          tr('register.step_two.title'),
          style: TextStyle(
              fontSize: 20, color: primary_3, fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Widget _buildDisplayPhone() {
    return Container(
      padding: const EdgeInsets.only(top: 0, bottom: 38),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 5),
              child: Text(
                "${widget.countryCode}",
                style: const TextStyle(
                    fontSize: 20, color: showdow, fontWeight: FontWeight.w700),
              ),
            ),
            Text(
              "${widget.phoneNumber?.substring(1)}",
              style: const TextStyle(
                  fontSize: 20, color: showdow, fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInputOTP() {
    final defaultPinTheme = PinTheme(
      width: 60,
      height: 60,
      textStyle: TextStyle(
          fontSize: 20,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.normal),
      decoration: BoxDecoration(
        border: Border(
            bottom: BorderSide(
          color: primary_3,
          width: 1.0,
        )),
      ),
    );

    return Container(
      margin: EdgeInsets.only(left: 30, right: 27),
      child: Pinput(
        controller: pinController,
        defaultPinTheme: defaultPinTheme,
        length: 4,
      ),
    );
  }

  Widget _buildOTPActions() {
    return Container(
      padding: EdgeInsets.only(top: 31, bottom: 28),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (!waitResendOtp) ...[
            GestureDetector(
              onTap: () {
                //OTP Sent again

                EasyLoading.show(
                  status: tr('loading'),
                  maskType: EasyLoadingMaskType.black,
                );

                context
                    .read<StepOnePageViewModel>()
                    .otpRequest(widget.phoneNumber)
                    .then((value) {
                  EasyLoading.dismiss();
                  tokenRes = value?.data?.token;
                }).onError((error, stackTrace) {
                  EasyLoading.dismiss();
                  EasyLoading.showError("${tr('error.no_process')} (G002)",
                      duration: const Duration(milliseconds: 700));
                });

                controller = CountdownTimerController(
                    endTime: DateTime.now().millisecondsSinceEpoch + 1000 * 60,
                    onEnd: onEnd);
                waitResendOtp = true;
                pinController.clear();
                setState(() {});
              },
              child: Text(
                tr('register.step_two.text_1'),
                style: const TextStyle(
                    decoration: TextDecoration.underline,
                    color: showdow,
                    fontSize: 13.0,
                    fontWeight: FontWeight.w400),
              ),
            )
          ] else ...[
            const Icon(
              Icons.cached,
              color: showdow,
            ),
            const Text(" "),
            Text(
              tr('register.step_two.text_3'),
              style: const TextStyle(
                color: showdow,
                fontSize: 14.0,
              ),
            ),
            CountdownTimer(
              controller: controller,
              widgetBuilder: (context, time) {
                return Text(
                  "${time?.min ?? '00'}:${time?.sec ?? '00'}",
                  style: const TextStyle(
                    color: showdow,
                    fontSize: 14.0,
                  ),
                );
              },
            ),
            const Text(" "),
          ]
        ],
      ),
    );
  }

  Widget _buildButtonNextStep() {
    return Container(
      width: 100.w,
      margin: EdgeInsets.only(top: 0, bottom: 18),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (pinController.text != null) {
            if (pinController.text.length >= 4) {
              //Step 1 check type
              if (widget.otprequestdata?.typeCustomer == "old") {
                //Step 2 OTP verify
                //Step 3 Verify signin
                context
                    .read<StepOnePageViewModel>()
                    .otpVerify(tokenRes, pinController.text)
                    .then((valueOTPVarify) {
                  if (valueOTPVarify?.data?.message != null) {
                    if (valueOTPVarify?.data?.message == "Code is correct.") {
                      context
                          .read<StepOnePageViewModel>()
                          .verifySignin(
                              widget.phoneNumber, tokenRes, pinController.text)
                          .then((value) {
                        if (value?.data?.token != null) {
                          context.read<ProfileViewModel>().setSignIn(true);
                          context
                              .read<ProfileViewModel>()
                              .setTokenCustome(value?.data?.token);
                          context
                                  .read<StepFourPageViewModel>()
                                  .phoneNumberSuccess =
                              "${widget.countryCode} ${widget.phoneNumber?.substring(1)}";
                          context
                              .read<ProfileViewModel>()
                              .setRegisComplete(true);
                          Navigator.of(context)
                              .popUntil(ModalRoute.withName("HomeRoute"));
                        } else {
                          EasyLoading.showError(
                              "${tr('error.no_process')} (G005)",
                              duration: const Duration(milliseconds: 700));
                        }
                      });
                    } else {
                      EasyLoading.showError("${tr('error.no_process')} (G004)",
                          duration: const Duration(milliseconds: 700));
                    }
                  } else {
                    EasyLoading.showError("${tr('error.no_process')} (G003)",
                        duration: const Duration(milliseconds: 700));
                  }
                });
              } else if (widget.otprequestdata?.typeCustomer == "new") {
                //Go to step tree
                //Step 2 OTP verify
                //Step 3 Input personal data
                context
                    .read<StepOnePageViewModel>()
                    .otpVerify(tokenRes, pinController.text)
                    .then((valueOTPVarify) {
                  if (valueOTPVarify?.data?.message != null) {
                    if (valueOTPVarify?.data?.message == "Code is correct.") {
                      context.router.push(StepTreeRoute(
                          countryCode: widget.countryCode,
                          phoneNumber: widget.phoneNumber));
                    } else {
                      EasyLoading.showError("${tr('error.no_process')} (G004)",
                          duration: const Duration(milliseconds: 700));
                    }
                  } else {
                    EasyLoading.showError("${tr('error.no_process')} (G003)",
                        duration: const Duration(milliseconds: 700));
                  }
                });
              } else {
                EasyLoading.showError("${tr('error.no_process')} (G001)",
                    duration: const Duration(milliseconds: 700));
              }
            } else {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
              Future.delayed(const Duration(milliseconds: 500), () {
                showOkAlertDialog(
                    context: context, message: tr('error.no_input_otp'));
              });
            }
          } else {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            Future.delayed(const Duration(milliseconds: 500), () {
              showOkAlertDialog(
                  context: context, message: tr('error.no_input_otp'));
            });
          }
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            width: 60.w,
            padding: EdgeInsets.only(top: 14, bottom: 14),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: primary_3)),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                tr('register.step_one.text_2'),
                style: const TextStyle(
                    fontSize: 16,
                    color: primary_3,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
