import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/registers/step_one/step_one_viewmodel.dart';
import 'package:workpresso/ui/term/term_viewmodel.dart';

import 'step_four_viewmodel.dart';

class StepFourPage extends StatefulWidget {
  const StepFourPage(
      {this.countryCode,
      this.phoneNumber,
      this.customerName,
      this.customerLastName,
      this.customerEmail,
      this.customerBD,
      Key? key})
      : super(key: key);

  final String? countryCode;
  final String? phoneNumber;
  final String? customerName;
  final String? customerLastName;
  final String? customerEmail;
  final String? customerBD;

  @override
  _StepFourPageState createState() => _StepFourPageState();
}

class _StepFourPageState extends State<StepFourPage> {
  TextEditingController? controllerInput = TextEditingController();
  bool checkTerm = false;
  String? detail = "";
  bool reload = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    context.read<TermViewModel>().getTerm().then((value) {
      if (value?.data?.detail != null) {
        detail = value?.data?.detail ?? "";
      }
      if (reload) {
        setState(() {
          reload = false;
        });
      }
    });
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_7,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
            titleBaseAppBar: tr('register.step_four.title'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SafeArea(
              child: Wrap(
            children: [
              _buildTitle(),
              _bulidContent(),
              _buildButtonNextStep(),
            ],
          )),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 0),
      width: 100.w,
      child: Container(
        constraints: BoxConstraints(
          minHeight: 395,
          maxHeight: 395,
        ),
        margin: EdgeInsets.only(left: 27, right: 27),
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Html(
              data: detail ?? "",
            ),
          ),
        ),
      ),
    );
  }

  Widget _bulidContent() {
    return Container(
      margin: EdgeInsets.only(top: 34, bottom: 22),
      child: GestureDetector(
        onTap: () {
          setState(() {
            if (checkTerm) {
              checkTerm = false;
            } else {
              checkTerm = true;
            }
          });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Icon(
                checkTerm == false
                    ? Icons.check_box_outline_blank
                    : Icons.check_box,
                color: primary_3,
                size: 20,
              ),
            ),
            Text(" "),
            Text(
              tr('register.step_four.text_1'),
              style: const TextStyle(
                  decoration: TextDecoration.underline,
                  decorationColor: primary_3,
                  fontSize: 13,
                  color: Colors.black,
                  fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildButtonNextStep() {
    return Container(
      width: 100.w,
      margin: EdgeInsets.only(top: 0, bottom: 18),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (checkTerm) {
            context
                .read<StepOnePageViewModel>()
                .registerCustomer(widget.customerName, widget.customerLastName,
                    widget.phoneNumber, widget.customerBD)
                .then((value) {
              if (value?.data?.token != null) {
                if (value?.data?.token != "") {
                  context.read<ProfileViewModel>().setSignIn(true);
                  context.read<StepFourPageViewModel>().phoneNumberSuccess =
                      "${widget.countryCode} ${widget.phoneNumber?.substring(1)}";
                  context.read<ProfileViewModel>().setRegisComplete(true);
                  Navigator.of(context)
                      .popUntil(ModalRoute.withName("HomeRoute"));
                } else {
                  EasyLoading.showError("${tr('error.no_process')} (G006)",
                      duration: const Duration(milliseconds: 700));
                }
              } else {
                EasyLoading.showError("${tr('error.no_process')} (G006)",
                    duration: const Duration(milliseconds: 700));
              }
            });
          }
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            width: 60.w,
            padding: EdgeInsets.only(top: 14, bottom: 14),
            decoration: BoxDecoration(
              color: checkTerm == false ? codeCountry : primary_3,
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.25),
                  blurRadius: 4,
                  offset: Offset(2, 3), // Shadow position
                ),
              ],
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                tr('register.step_four.text_2'),
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
