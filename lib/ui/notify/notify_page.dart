import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/ui/home/home_viewmodel.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class NotifyPage extends StatefulWidget {
  const NotifyPage({Key? key}) : super(key: key);

  @override
  _NotifyPageState createState() => _NotifyPageState();
}

class _NotifyPageState extends State<NotifyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowAppBar: true,
        centerTitle: true,
        centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
        titleBaseAppBar: tr('menu.notify'),
        rightStatusBaseAppBar: RightStatusBaseAppBar.profile,
        rightStatusBaseAppBarVisibility:
            context.read<ProfileViewModel>().isSignInLocal,
        onRightPressCallback: () {
          context.read<HomeViewModel>().setSelectMenuBar(2);
        },
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 25),
          width: 100.w,
          child: Align(
            alignment: Alignment.topCenter,
            child: Text(
              "coming soon....",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
    );
  }
}
