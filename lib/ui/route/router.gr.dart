// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i20;
import 'package:flutter/material.dart' as _i21;

import '../../models/customer/otp_request.dart' as _i22;
import '../coupon/coupon_page.dart' as _i4;
import '../forgotPassword/forgot_password_page.dart' as _i15;
import '../forgotPassword/forgot_password_send_page.dart' as _i16;
import '../home/home_page.dart' as _i3;
import '../notify/notify_page.dart' as _i6;
import '../other/other_page.dart' as _i7;
import '../policy/policy_page.dart' as _i9;
import '../profile/profile_page.dart' as _i5;
import '../registers/step_four/step_four_page.dart' as _i13;
import '../registers/step_one/step_one_page.dart' as _i10;
import '../registers/step_tree/step_tree_page.dart' as _i12;
import '../registers/step_two/step_two_page.dart' as _i11;
import '../selectLanguage/select_language_page.dart' as _i2;
import '../signIn/sign_in_page.dart' as _i14;
import '../signUp/account_verify_page.dart' as _i19;
import '../signUp/sign_up_page.dart' as _i17;
import '../splash/splash_screen_page.dart' as _i1;
import '../term/term_page.dart' as _i8;
import '../tutorial/tutorial_page.dart' as _i18;

class AppRouter extends _i20.RootStackRouter {
  AppRouter([_i21.GlobalKey<_i21.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i20.PageFactory> pagesMap = {
    SplashScreenRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.SplashScreenPage(),
      );
    },
    SelectLanguageRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i2.SelectLanguagePage(),
      );
    },
    HomeRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i3.HomePage(),
      );
    },
    CouponRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i4.CouponPage(),
      );
    },
    ProfileRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i5.ProfilePage(),
      );
    },
    NotifyRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i6.NotifyPage(),
      );
    },
    OtherRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i7.OtherPage(),
      );
    },
    TermRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i8.TermPage(),
      );
    },
    PolicyRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i9.PolicyPage(),
      );
    },
    StepOneRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i10.StepOnePage(),
      );
    },
    StepTwoRoute.name: (routeData) {
      final args = routeData.argsAs<StepTwoRouteArgs>(
          orElse: () => const StepTwoRouteArgs());
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i11.StepTwoPage(
          countryCode: args.countryCode,
          phoneNumber: args.phoneNumber,
          otprequestdata: args.otprequestdata,
          key: args.key,
        ),
      );
    },
    StepTreeRoute.name: (routeData) {
      final args = routeData.argsAs<StepTreeRouteArgs>(
          orElse: () => const StepTreeRouteArgs());
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i12.StepTreePage(
          countryCode: args.countryCode,
          phoneNumber: args.phoneNumber,
          key: args.key,
        ),
      );
    },
    StepFourRoute.name: (routeData) {
      final args = routeData.argsAs<StepFourRouteArgs>(
          orElse: () => const StepFourRouteArgs());
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i13.StepFourPage(
          countryCode: args.countryCode,
          phoneNumber: args.phoneNumber,
          customerName: args.customerName,
          customerLastName: args.customerLastName,
          customerEmail: args.customerEmail,
          customerBD: args.customerBD,
          key: args.key,
        ),
      );
    },
    SignInRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i14.SignInPage(),
      );
    },
    ForgotPasswordRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i15.ForgotPasswordPage(),
      );
    },
    ForgotPasswordSendRoute.name: (routeData) {
      final args = routeData.argsAs<ForgotPasswordSendRouteArgs>(
          orElse: () => const ForgotPasswordSendRouteArgs());
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i16.ForgotPasswordSendPage(
          emailSend: args.emailSend,
          key: args.key,
        ),
      );
    },
    SignUpRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i17.SignUpPage(),
      );
    },
    TutorialRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i18.TutorialPage(),
      );
    },
    AccountVerifyRoute.name: (routeData) {
      return _i20.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i19.AccountVerifyPage(),
      );
    },
  };

  @override
  List<_i20.RouteConfig> get routes => [
        _i20.RouteConfig(
          SplashScreenRoute.name,
          path: '/',
        ),
        _i20.RouteConfig(
          SelectLanguageRoute.name,
          path: '/select-language-page',
        ),
        _i20.RouteConfig(
          HomeRoute.name,
          path: '/home-page',
        ),
        _i20.RouteConfig(
          CouponRoute.name,
          path: '/coupon-page',
        ),
        _i20.RouteConfig(
          ProfileRoute.name,
          path: '/profile-page',
        ),
        _i20.RouteConfig(
          NotifyRoute.name,
          path: '/notify-page',
        ),
        _i20.RouteConfig(
          OtherRoute.name,
          path: '/other-page',
        ),
        _i20.RouteConfig(
          TermRoute.name,
          path: '/term-page',
        ),
        _i20.RouteConfig(
          PolicyRoute.name,
          path: '/policy-page',
        ),
        _i20.RouteConfig(
          StepOneRoute.name,
          path: '/step-one-page',
        ),
        _i20.RouteConfig(
          StepTwoRoute.name,
          path: '/step-two-page',
        ),
        _i20.RouteConfig(
          StepTreeRoute.name,
          path: '/step-tree-page',
        ),
        _i20.RouteConfig(
          StepFourRoute.name,
          path: '/step-four-page',
        ),
        _i20.RouteConfig(
          SignInRoute.name,
          path: '/sign-in-page',
        ),
        _i20.RouteConfig(
          ForgotPasswordRoute.name,
          path: '/forgot-password-page',
        ),
        _i20.RouteConfig(
          ForgotPasswordSendRoute.name,
          path: '/forgot-password-send-page',
        ),
        _i20.RouteConfig(
          SignUpRoute.name,
          path: '/sign-up-page',
        ),
        _i20.RouteConfig(
          TutorialRoute.name,
          path: '/tutorial-page',
        ),
        _i20.RouteConfig(
          AccountVerifyRoute.name,
          path: '/account-verify-page',
        ),
      ];
}

/// generated route for
/// [_i1.SplashScreenPage]
class SplashScreenRoute extends _i20.PageRouteInfo<void> {
  const SplashScreenRoute()
      : super(
          SplashScreenRoute.name,
          path: '/',
        );

  static const String name = 'SplashScreenRoute';
}

/// generated route for
/// [_i2.SelectLanguagePage]
class SelectLanguageRoute extends _i20.PageRouteInfo<void> {
  const SelectLanguageRoute()
      : super(
          SelectLanguageRoute.name,
          path: '/select-language-page',
        );

  static const String name = 'SelectLanguageRoute';
}

/// generated route for
/// [_i3.HomePage]
class HomeRoute extends _i20.PageRouteInfo<void> {
  const HomeRoute()
      : super(
          HomeRoute.name,
          path: '/home-page',
        );

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i4.CouponPage]
class CouponRoute extends _i20.PageRouteInfo<void> {
  const CouponRoute()
      : super(
          CouponRoute.name,
          path: '/coupon-page',
        );

  static const String name = 'CouponRoute';
}

/// generated route for
/// [_i5.ProfilePage]
class ProfileRoute extends _i20.PageRouteInfo<void> {
  const ProfileRoute()
      : super(
          ProfileRoute.name,
          path: '/profile-page',
        );

  static const String name = 'ProfileRoute';
}

/// generated route for
/// [_i6.NotifyPage]
class NotifyRoute extends _i20.PageRouteInfo<void> {
  const NotifyRoute()
      : super(
          NotifyRoute.name,
          path: '/notify-page',
        );

  static const String name = 'NotifyRoute';
}

/// generated route for
/// [_i7.OtherPage]
class OtherRoute extends _i20.PageRouteInfo<void> {
  const OtherRoute()
      : super(
          OtherRoute.name,
          path: '/other-page',
        );

  static const String name = 'OtherRoute';
}

/// generated route for
/// [_i8.TermPage]
class TermRoute extends _i20.PageRouteInfo<void> {
  const TermRoute()
      : super(
          TermRoute.name,
          path: '/term-page',
        );

  static const String name = 'TermRoute';
}

/// generated route for
/// [_i9.PolicyPage]
class PolicyRoute extends _i20.PageRouteInfo<void> {
  const PolicyRoute()
      : super(
          PolicyRoute.name,
          path: '/policy-page',
        );

  static const String name = 'PolicyRoute';
}

/// generated route for
/// [_i10.StepOnePage]
class StepOneRoute extends _i20.PageRouteInfo<void> {
  const StepOneRoute()
      : super(
          StepOneRoute.name,
          path: '/step-one-page',
        );

  static const String name = 'StepOneRoute';
}

/// generated route for
/// [_i11.StepTwoPage]
class StepTwoRoute extends _i20.PageRouteInfo<StepTwoRouteArgs> {
  StepTwoRoute({
    String? countryCode,
    String? phoneNumber,
    _i22.OtpRequest? otprequestdata,
    _i21.Key? key,
  }) : super(
          StepTwoRoute.name,
          path: '/step-two-page',
          args: StepTwoRouteArgs(
            countryCode: countryCode,
            phoneNumber: phoneNumber,
            otprequestdata: otprequestdata,
            key: key,
          ),
        );

  static const String name = 'StepTwoRoute';
}

class StepTwoRouteArgs {
  const StepTwoRouteArgs({
    this.countryCode,
    this.phoneNumber,
    this.otprequestdata,
    this.key,
  });

  final String? countryCode;

  final String? phoneNumber;

  final _i22.OtpRequest? otprequestdata;

  final _i21.Key? key;

  @override
  String toString() {
    return 'StepTwoRouteArgs{countryCode: $countryCode, phoneNumber: $phoneNumber, otprequestdata: $otprequestdata, key: $key}';
  }
}

/// generated route for
/// [_i12.StepTreePage]
class StepTreeRoute extends _i20.PageRouteInfo<StepTreeRouteArgs> {
  StepTreeRoute({
    String? countryCode,
    String? phoneNumber,
    _i21.Key? key,
  }) : super(
          StepTreeRoute.name,
          path: '/step-tree-page',
          args: StepTreeRouteArgs(
            countryCode: countryCode,
            phoneNumber: phoneNumber,
            key: key,
          ),
        );

  static const String name = 'StepTreeRoute';
}

class StepTreeRouteArgs {
  const StepTreeRouteArgs({
    this.countryCode,
    this.phoneNumber,
    this.key,
  });

  final String? countryCode;

  final String? phoneNumber;

  final _i21.Key? key;

  @override
  String toString() {
    return 'StepTreeRouteArgs{countryCode: $countryCode, phoneNumber: $phoneNumber, key: $key}';
  }
}

/// generated route for
/// [_i13.StepFourPage]
class StepFourRoute extends _i20.PageRouteInfo<StepFourRouteArgs> {
  StepFourRoute({
    String? countryCode,
    String? phoneNumber,
    String? customerName,
    String? customerLastName,
    String? customerEmail,
    String? customerBD,
    _i21.Key? key,
  }) : super(
          StepFourRoute.name,
          path: '/step-four-page',
          args: StepFourRouteArgs(
            countryCode: countryCode,
            phoneNumber: phoneNumber,
            customerName: customerName,
            customerLastName: customerLastName,
            customerEmail: customerEmail,
            customerBD: customerBD,
            key: key,
          ),
        );

  static const String name = 'StepFourRoute';
}

class StepFourRouteArgs {
  const StepFourRouteArgs({
    this.countryCode,
    this.phoneNumber,
    this.customerName,
    this.customerLastName,
    this.customerEmail,
    this.customerBD,
    this.key,
  });

  final String? countryCode;

  final String? phoneNumber;

  final String? customerName;

  final String? customerLastName;

  final String? customerEmail;

  final String? customerBD;

  final _i21.Key? key;

  @override
  String toString() {
    return 'StepFourRouteArgs{countryCode: $countryCode, phoneNumber: $phoneNumber, customerName: $customerName, customerLastName: $customerLastName, customerEmail: $customerEmail, customerBD: $customerBD, key: $key}';
  }
}

/// generated route for
/// [_i14.SignInPage]
class SignInRoute extends _i20.PageRouteInfo<void> {
  const SignInRoute()
      : super(
          SignInRoute.name,
          path: '/sign-in-page',
        );

  static const String name = 'SignInRoute';
}

/// generated route for
/// [_i15.ForgotPasswordPage]
class ForgotPasswordRoute extends _i20.PageRouteInfo<void> {
  const ForgotPasswordRoute()
      : super(
          ForgotPasswordRoute.name,
          path: '/forgot-password-page',
        );

  static const String name = 'ForgotPasswordRoute';
}

/// generated route for
/// [_i16.ForgotPasswordSendPage]
class ForgotPasswordSendRoute
    extends _i20.PageRouteInfo<ForgotPasswordSendRouteArgs> {
  ForgotPasswordSendRoute({
    String? emailSend,
    _i21.Key? key,
  }) : super(
          ForgotPasswordSendRoute.name,
          path: '/forgot-password-send-page',
          args: ForgotPasswordSendRouteArgs(
            emailSend: emailSend,
            key: key,
          ),
        );

  static const String name = 'ForgotPasswordSendRoute';
}

class ForgotPasswordSendRouteArgs {
  const ForgotPasswordSendRouteArgs({
    this.emailSend,
    this.key,
  });

  final String? emailSend;

  final _i21.Key? key;

  @override
  String toString() {
    return 'ForgotPasswordSendRouteArgs{emailSend: $emailSend, key: $key}';
  }
}

/// generated route for
/// [_i17.SignUpPage]
class SignUpRoute extends _i20.PageRouteInfo<void> {
  const SignUpRoute()
      : super(
          SignUpRoute.name,
          path: '/sign-up-page',
        );

  static const String name = 'SignUpRoute';
}

/// generated route for
/// [_i18.TutorialPage]
class TutorialRoute extends _i20.PageRouteInfo<void> {
  const TutorialRoute()
      : super(
          TutorialRoute.name,
          path: '/tutorial-page',
        );

  static const String name = 'TutorialRoute';
}

/// generated route for
/// [_i19.AccountVerifyPage]
class AccountVerifyRoute extends _i20.PageRouteInfo<void> {
  const AccountVerifyRoute()
      : super(
          AccountVerifyRoute.name,
          path: '/account-verify-page',
        );

  static const String name = 'AccountVerifyRoute';
}
