import 'package:auto_route/auto_route.dart';
import 'package:workpresso/ui/coupon/coupon_page.dart';
import 'package:workpresso/ui/forgotPassword/forgot_password_page.dart';
import 'package:workpresso/ui/forgotPassword/forgot_password_send_page.dart';
import 'package:workpresso/ui/home/home_page.dart';
import 'package:workpresso/ui/notify/notify_page.dart';
import 'package:workpresso/ui/other/other_page.dart';
import 'package:workpresso/ui/policy/policy_page.dart';
import 'package:workpresso/ui/profile/profile_page.dart';
import 'package:workpresso/ui/registers/step_four/step_four_page.dart';
import 'package:workpresso/ui/registers/step_one/step_one_page.dart';
import 'package:workpresso/ui/registers/step_tree/step_tree_page.dart';
import 'package:workpresso/ui/registers/step_two/step_two_page.dart';
import 'package:workpresso/ui/selectLanguage/select_language_page.dart';
import 'package:workpresso/ui/signIn/sign_in_page.dart';
import 'package:workpresso/ui/signUp/account_verify_page.dart';
import 'package:workpresso/ui/signUp/sign_up_page.dart';
import 'package:workpresso/ui/splash/splash_screen_page.dart';
import 'package:workpresso/ui/term/term_page.dart';
import 'package:workpresso/ui/tutorial/tutorial_page.dart';

@MaterialAutoRouter(replaceInRouteName: 'Page,Route', routes: <AutoRoute>[
  AutoRoute(page: SplashScreenPage, initial: true),
  AutoRoute(page: SelectLanguagePage),
  AutoRoute(page: HomePage),
  AutoRoute(page: CouponPage),
  AutoRoute(page: ProfilePage),
  AutoRoute(page: NotifyPage),
  AutoRoute(page: OtherPage),
  AutoRoute(page: TermPage),
  AutoRoute(page: PolicyPage),
  AutoRoute(page: StepOnePage),
  AutoRoute(page: StepTwoPage),
  AutoRoute(page: StepTreePage),
  AutoRoute(page: StepFourPage),
  AutoRoute(page: SignInPage),
  AutoRoute(page: ForgotPasswordPage),
  AutoRoute(page: ForgotPasswordSendPage),
  AutoRoute(page: SignUpPage),
  AutoRoute(page: TutorialPage),
  AutoRoute(page: AccountVerifyPage),
])
class $AppRouter {}
