import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_constants.dart';
import 'package:workpresso/constants/app_font.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';

class SelectLanguagePage extends StatefulWidget {
  const SelectLanguagePage({Key? key}) : super(key: key);

  @override
  _SelectLanguagePageState createState() => _SelectLanguagePageState();
}

class _SelectLanguagePageState extends State<SelectLanguagePage> {
  String? currentLang = 'th';
  String? selectLang;

  @override
  void initState() {
    super.initState();
    selectLang = null;
  }

  @override
  Widget build(BuildContext context) {
    if (selectLang == null) {
      currentLang = context.locale.toString();
    } else {
      currentLang = selectLang;
    }

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Container(
          color: primary_1,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 100.w,
                  child: SizedBox(
                      width: 220,
                      height: 220,
                      child: Image.asset(AppAssets.logo)),
                ),
                for (var element in context.supportedLocales)
                  _displayLanguage(element),
                constSpacing5,
                _btnEnter()
              ]),
        ),
      ),
    );
  }

  Widget _displayLanguage(Locale? locale) {
    Widget result = Container();
    if (locale != null) {
      bool isCheck = false;
      if (currentLang == locale.toString()) {
        isCheck = true;
      } else {
        isCheck = false;
      }
      result = GestureDetector(
        onTap: () {
          selectLang = locale.toString();
          setState(() {});
        },
        child: Container(
          margin: const EdgeInsets.all(5),
          width: 301,
          height: 61,
          decoration: BoxDecoration(
              color: currentLang == locale.toString() ? primary_2 : primary_5,
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                    blurRadius: 4,
                    color: showdow.withOpacity(0.25),
                    offset: const Offset(2, 2))
              ]),
          child: Padding(
            padding: EdgeInsets.all(5),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(23, 0, 0, 0),
                  child: Image.asset(
                    locale.languageCode == "th"
                        ? AppAssets.flag_th
                        : AppAssets.flag_en,
                    width: 41,
                    height: 27,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(31, 0, 0, 0),
                  child: Text(
                    locale.languageCode == "th"
                        ? tr('languages.th')
                        : tr('languages.en'),
                    style: BaseTextStyle().custom(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Spacer(),
                _radioSelected(isCheck),
              ],
            ),
          ),
        ),
      );
    }
    return result;
  }

  Widget _radioSelected(bool? enable) {
    Widget result = Container();
    result = Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 26, 0),
      child: Container(
        width: 15,
        height: 15,
        decoration: BoxDecoration(
          color: enable! ? primary_3 : primary_4,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Visibility(
          visible: enable,
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: Container(
              width: 7.5,
              height: 7.5,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ),
      ),
    );
    return result;
  }

  Widget _btnEnter() {
    Widget result = SizedBox();
    result = SizedBox(
      width: 256,
      height: 52,
      child: ElevatedButton(
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor: MaterialStateProperty.all<Color>(primary_3),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ))),
          onPressed: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            context.setLocale(Locale(currentLang.toString())); //Set new lang
            prefs.setBool(firstInstall, true);
            context.router.push(HomeRoute());
          },
          child: Text(tr('enter'), style: const TextStyle(fontSize: 16))),
    );
    return result;
  }
}
