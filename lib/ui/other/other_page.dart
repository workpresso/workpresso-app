import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/home/home_viewmodel.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/utils/utils.dart';

class OtherPage extends StatefulWidget {
  const OtherPage({Key? key}) : super(key: key);

  @override
  _OtherPageState createState() => _OtherPageState();
}

class _OtherPageState extends State<OtherPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowAppBar: true,
        centerTitle: true,
        centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
        titleBaseAppBar: tr('menu.other'),
        rightStatusBaseAppBar: RightStatusBaseAppBar.profile,
        rightStatusBaseAppBarVisibility:
            context.read<ProfileViewModel>().isSignInLocal,
        onRightPressCallback: () {
          context.read<ProfileViewModel>().getCustomerProfile();
          context.read<HomeViewModel>().setSelectMenuBar(2);
        },
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(bottom: 30),
          child: Wrap(
            children: [
              _buildLogoSection(),
              _buildSectionOne(),
              _buildSectionSecond(),
              _buildSectionTree(),
            ],
          ),
        ),
      )),
    );
  }

  Widget _buildLogoSection() {
    return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      width: 100.w,
      height: 142,
      color: primary_6,
      child: Image.asset(
        AppAssets.logo,
        width: 154,
        height: 154,
      ),
    );
  }

  Widget _buildSectionOne() {
    return Container(
        margin: const EdgeInsets.only(top: 5, bottom: 5, left: 35, right: 35),
        width: 100.w,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 17),
              child: Text(
                tr('other.contact.title'),
                style: const TextStyle(
                  color: primary_3,
                  fontSize: 13,
                ),
              ),
            ),
            Row(
              children: [
                _itemMenu(AppAssets.other_line, tr('other.contact.line'), () {
                  Utils().openUrl(
                      "https://line.me/R/home/public/main?id=golfgogo");
                }),
                Spacer(),
                _itemMenu(
                    AppAssets.other_facebook, tr('other.contact.facebook'), () {
                  if (Utils().getPlatformOS() == "android") {
                    Utils().openUrl("fb://page/182841558988898");
                  } else {
                    Utils().openUrl("fb://profile/182841558988898");
                  }
                }),
                Spacer(),
                _itemMenu(AppAssets.other_youtube, tr('other.contact.youtube'),
                    () {
                  if (Utils().getPlatformOS() == "android") {
                    Utils().openUrl(
                        "https://www.youtube.com/channel/UCbwbyzzlTn82rl6bdM5v5_g");
                  } else {
                    Utils()
                        .openUrl("youtube://channel/UCbwbyzzlTn82rl6bdM5v5_g");
                  }
                }),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, bottom: 10),
              child: Image.asset(AppAssets.other_line_hr),
            )
          ],
        ));
  }

  Widget _buildSectionSecond() {
    return Container(
        margin: const EdgeInsets.only(top: 5, bottom: 5, left: 35, right: 35),
        width: 100.w,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 17),
              child: Text(
                tr('other.contact_us.title'),
                style: const TextStyle(
                  color: primary_3,
                  fontSize: 13,
                ),
              ),
            ),
            Row(
              children: [
                _itemMenu(AppAssets.other_email, tr('other.contact_us.email'),
                    () {
                  Utils().openUrl("mailto:admin@ggg-inter.com");
                }),
                Spacer(),
                _itemMenu(
                    AppAssets.other_call, tr('other.contact_us.call_center'),
                    () {
                  Utils().openCallCenter();
                }),
                Spacer(),
                _itemMenu(
                    AppAssets.other_noti, tr('other.contact_us.notify'), null),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, bottom: 10),
              child: Image.asset(AppAssets.other_line_hr),
            )
          ],
        ));
  }

  Widget _buildSectionTree() {
    return Container(
        margin: const EdgeInsets.only(top: 5, bottom: 0, left: 35, right: 35),
        width: 100.w,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 17),
              child: Text(
                tr('other.legal_requirements.title'),
                style: const TextStyle(
                  color: primary_3,
                  fontSize: 13,
                ),
              ),
            ),
            Row(
              children: [
                _itemMenu(AppAssets.other_condition,
                    tr('other.legal_requirements.condition'), () {
                  context.router.push(const TermRoute());
                }),
                Spacer(),
                _itemMenu(AppAssets.other_policy,
                    tr('other.legal_requirements.policy'), () {
                  context.router.push(const PolicyRoute());
                }),
                Spacer(),
                Spacer(),
              ],
            ),
            Visibility(
              visible: false,
              child: Container(
                margin: const EdgeInsets.only(top: 31, bottom: 18),
                child: Image.asset(AppAssets.other_line_hr),
              ),
            )
          ],
        ));
  }

  Widget _itemMenu(String images, String title, Function()? actionTab) {
    return Container(
      child: GestureDetector(
        onTap: actionTab,
        child: Column(
          children: [
            Image.asset(
              images,
              width: 50,
              height: 50,
            ),
            Padding(
              padding: EdgeInsets.only(top: 9),
              child: SizedBox(
                width: 75,
                height: 35,
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    title,
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: showdow,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
