import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/customer/upload_image_profile.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/customer_service.dart';

@injectable
class ProfileViewModel extends ChangeNotifier {
  bool regisComplete = false; //Use check register all step complete or sigin
  bool isSignOut = false;
  bool isSignInLocal = false;
  CustomerProfile? _customerProfileData;
  CustomerProfile? get customerProfileData => _customerProfileData;

  Future<void> checkSignIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isSignInLocal = prefs.getBool(isSignIn) ?? false;
  }

  Future<void> setSignIn(bool setVal) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(isSignIn, setVal);
  }

  Future<void> setRegisComplete(bool value) async {
    regisComplete = value;
    notifyListeners();
  }

  Future<void> setSignOut(bool value) async {
    isSignOut = value;
    notifyListeners();
  }

  Future<void> setTokenCustome(String? token) async {
    final prefs = await SharedPreferences.getInstance();
    if (token != null) {
      if (token.isNotEmpty) {
        prefs.setString(tokenCustomer, token);
      }
    }
  }

  Future<void> clearTokenCustome() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(tokenCustomer);
  }

  Future<void> clearSignIn() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(isSignIn);
  }

  Future<String?> getTokenCustome() async {
    String? result;
    final prefs = await SharedPreferences.getInstance();
    result = prefs.getString(tokenCustomer);
    return result;
  }

  Future<void> getCustomerProfile() async {
    Resource<CustomerProfile?>? result;
    result = await getIt<CustomerService>().getCustomerProfile();
    _customerProfileData = result?.data;
    notifyListeners();
  }

  Future<void> signOut() async {
    _customerProfileData = null;
    isSignInLocal = false;
    clearTokenCustome();
    clearSignIn();
  }

  Future<Resource<UploadImageProfile?>?> uploadImageProfile(
      String? partImage, String? nameImage) async {
    Resource<UploadImageProfile?>? result;
    result =
        await getIt<CustomerService>().uploadImageProfile(partImage, nameImage);
    return result;
  }

  Future<Resource<void>?> updateProfile(String? firstName, String? lastName,
      String? image, String? birthDay, String? phoneNo) async {
    Resource<void>? result;
    result = await getIt<CustomerService>()
        .updateProfile(firstName, lastName, image, birthDay, phoneNo);
    return result;
  }
}
