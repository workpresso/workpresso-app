import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:workpresso/models/customer/customer_profile.dart';

import 'profile_viewmodel.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  CustomerProfile? customerProfileData;
  final ImagePicker _picker = ImagePicker();
  TextEditingController? controllerInputName = TextEditingController();
  TextEditingController? controllerInputLastName = TextEditingController();
  TextEditingController? controllerInputPhone = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    customerProfileData = context.read<ProfileViewModel>().customerProfileData;
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
    );
  }
}
