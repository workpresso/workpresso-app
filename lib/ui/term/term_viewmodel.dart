import 'package:flutter/foundation.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/term_policy/term.dart';
import 'package:workpresso/service/term_policy_service.dart';
import 'package:injectable/injectable.dart';

@injectable
class TermViewModel extends ChangeNotifier {
  Future<Resource<TermData?>?> getTerm() async {
    Resource<TermData?>? result;
    result = await getIt<TermPolicyService>().getTerm();
    return result;
  }
}
