import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/ui/term/term_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class TermPage extends StatefulWidget {
  const TermPage({Key? key}) : super(key: key);

  @override
  _TermPageState createState() => _TermPageState();
}

class _TermPageState extends State<TermPage> {
  String? title = tr('other.legal_requirements.condition');
  String? detail = "";
  bool reload = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    context.read<TermViewModel>().getTerm().then((value) {
      if (value?.data?.title != null) {
        title = value?.data?.title ?? "";
      }
      if (value?.data?.detail != null) {
        detail = value?.data?.detail ?? "";
      }
      if (reload) {
        setState(() {
          reload = false;
        });
      }
    });
    return Scaffold(
      appBar: BaseAppBar(
        isShowAppBar: true,
        centerTitle: true,
        leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
        onLeftPressCallback: () {
          Navigator.pop(context);
        },
        centerStatusBaseAppBar: CenterStatusBaseAppBar.title,
        titleBaseAppBar: title ?? "",
        rightStatusBaseAppBar: RightStatusBaseAppBar.none,
        rightStatusBaseAppBarVisibility: true,
      ),
      body: SafeArea(
          child: Column(
        children: [
          _getContent(),
        ],
      )),
    );
  }

  Widget _getContent() {
    return Container(
      height: 82.h,
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(top: 26, left: 27, right: 30),
            child: Html(
              data: detail ?? "",
            ),
          ),
        ),
      ),
    );
  }
}
