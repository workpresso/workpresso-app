import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/route/router.gr.dart';

class AccountVerifyPage extends StatefulWidget {
  const AccountVerifyPage({Key? key}) : super(key: key);

  @override
  _AccountVerifyPageState createState() => _AccountVerifyPageState();
}

class _AccountVerifyPageState extends State<AccountVerifyPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.none,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
                child: Column(
              children: [
                _buildTitle(),
                _buildBack(),
              ],
            )),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 100, bottom: 30),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 25),
              child: Image.asset(
                AppAssets.icon_verify,
                width: 91,
                height: 91,
              ),
            ),
            Text(
              tr('sign_up.text_11'),
              style: const TextStyle(
                  fontSize: 32, color: primary_3, fontWeight: FontWeight.w400),
            ),
            Container(
              width: 90.w,
              child: Wrap(
                alignment: WrapAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: Text(
                      tr('sign_up.text_12'),
                      style: const TextStyle(
                          fontSize: 15,
                          color: showdow,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBack() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 45, top: 10),
        width: 100.w,
        child: Wrap(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                tr('sign_up.text_13'),
                style: TextStyle(color: textTerms, fontSize: 15),
              ),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const SignInRoute());
              },
              child: Text(
                tr('sign_up.text_14'),
                style: TextStyle(color: primary_10, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCreateAccount() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 45, top: 10),
        width: 100.w,
        child: Wrap(
          children: [
            Text(
              tr('sign_in.text_6'),
              style: TextStyle(color: textTerms, fontSize: 15),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const StepOneRoute());
              },
              child: Text(
                tr('sign_in.text_7'),
                style: TextStyle(color: primary_10, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
