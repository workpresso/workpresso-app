import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/ui/signUp/sign_up_viewmodel.dart';
import 'package:workpresso/utils/utils.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool _isHiddenPass = true;
  bool _isHiddenPassConfirm = true;
  bool _isResult = false;
  bool _isAccept = false;
  bool _isBTNEnable = false;
  String _strResult = "";
  bool _isShowPassword = false;
  bool _isShowPasswordConfirm = false;
  TextEditingController? controllerInputFirstName = TextEditingController();
  TextEditingController? controllerInputLastName = TextEditingController();
  TextEditingController? controllerInputEmail = TextEditingController();
  TextEditingController? controllerInputPassword = TextEditingController();
  TextEditingController? controllerInputPasswordConfirm =
      TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.back,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
                child: Column(
              children: [
                _buildTitle(),
                _buildInputSignIn(),
                _buildResultOutPut(),
                _buildButtonSignInAndRememPass(),
                Container(
                  width: 90.w,
                  height: 1,
                  color: primary_3,
                ),
                _buildCreateAccount(),
              ],
            )),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: const EdgeInsets.only(top: 0, bottom: 30),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 25),
              child: Image.asset(
                AppAssets.logo,
                width: 207,
                height: 86,
              ),
            ),
            Text(
              tr('sign_up.title'),
              style: const TextStyle(
                  fontSize: 32, color: primary_3, fontWeight: FontWeight.w400),
            ),
            Text(
              tr('sign_up.des'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildResultOutPut() {
    return Visibility(
      visible: _isResult,
      child: Container(
        padding: const EdgeInsets.only(top: 15, left: 30),
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            const Icon(
              Icons.info,
              color: primary_12,
            ),
            Text(
              (_strResult),
              style: const TextStyle(
                  fontSize: 15, color: primary_12, fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInputSignIn() {
    return Container(
      margin: const EdgeInsets.only(left: 30, right: 27),
      child: Column(
        children: [
          Container(
            width: 85.w,
            padding: const EdgeInsets.only(left: 5),
            child: Text(
              tr('sign_up.text_9'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: const EdgeInsets.all(5),
            // height: 15,
            child: TextField(
              controller: controllerInputFirstName,
              keyboardType: TextInputType.emailAddress,
              onChanged: (content) {
                setState(() {
                  _checkBTNEnable();
                });
              },
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(
                    fontSize: 14.0,
                    color: codeCountry,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Container(
            width: 90.w,
            height: 2.h,
          ),
          Container(
            width: 85.w,
            padding: const EdgeInsets.only(left: 5),
            child: Text(
              tr('sign_up.text_10'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: const EdgeInsets.all(5),
            // height: 15,
            child: TextField(
              controller: controllerInputLastName,
              keyboardType: TextInputType.emailAddress,
              onChanged: (content) {
                setState(() {
                  _checkBTNEnable();
                });
              },
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(
                    fontSize: 14.0,
                    color: codeCountry,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Container(
            width: 90.w,
            height: 2.h,
          ),
          Container(
            width: 85.w,
            padding: const EdgeInsets.only(left: 5),
            child: Text(
              tr('sign_up.text_1'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: const EdgeInsets.all(5),
            // height: 15,
            child: TextField(
              controller: controllerInputEmail,
              keyboardType: TextInputType.emailAddress,
              onChanged: (content) {
                setState(() {
                  _checkBTNEnable();
                });
              },
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(
                    fontSize: 14.0,
                    color: codeCountry,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Container(
            width: 90.w,
            height: 2.h,
          ),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  tr('sign_up.text_2'),
                  style: const TextStyle(
                      fontSize: 15,
                      color: showdow,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Spacer(),
            ],
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: const EdgeInsets.all(5),
            // height: 15,
            child: Wrap(
              children: [
                Container(
                  width: 75.w,
                  height: 3.5.h,
                  child: TextField(
                    obscureText: _isHiddenPass,
                    controller: controllerInputPassword,
                    onChanged: (content) {
                      setState(() {
                        _checkBTNEnable();
                        if (content.isEmpty) {
                          _isShowPassword = false;
                        } else {
                          _isShowPassword = true;
                        }
                      });
                    },
                    keyboardType: TextInputType.text,
                    inputFormatters: [LengthLimitingTextInputFormatter(20)],
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                          fontSize: 14.0,
                          color: codeCountry,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 3),
                  child: Visibility(
                    visible: _isShowPassword,
                    child: InkWell(
                      onTap: _togglePasswordView,
                      child: Icon(
                        _isHiddenPass ? Icons.visibility : Icons.visibility_off,
                        color: primary_3,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 90.w,
            height: 2.h,
          ),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  tr('sign_up.text_3'),
                  style: const TextStyle(
                      fontSize: 15,
                      color: showdow,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const Spacer(),
            ],
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: const EdgeInsets.all(5),
            // height: 15,
            child: Wrap(
              children: [
                Container(
                  width: 75.w,
                  height: 3.5.h,
                  child: TextField(
                    obscureText: _isHiddenPassConfirm,
                    controller: controllerInputPasswordConfirm,
                    keyboardType: TextInputType.text,
                    onChanged: (content) {
                      setState(() {
                        _checkBTNEnable();
                        if (content.isEmpty) {
                          _isShowPasswordConfirm = false;
                        } else {
                          _isShowPasswordConfirm = true;
                        }
                      });
                    },
                    inputFormatters: [LengthLimitingTextInputFormatter(20)],
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                          fontSize: 14.0,
                          color: codeCountry,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 3),
                  child: Visibility(
                    visible: _isShowPasswordConfirm,
                    child: InkWell(
                      onTap: _togglePasswordConfirmView,
                      child: Icon(
                        _isHiddenPassConfirm
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: primary_3,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtonSignInAndRememPass() {
    return Container(
      width: 100.w,
      margin: const EdgeInsets.only(top: 18, bottom: 18, left: 30, right: 25),
      child: Row(
        children: [
          Container(
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      if (_isAccept) {
                        _isAccept = false;
                      } else {
                        _isAccept = true;
                      }
                      _checkBTNEnable();
                    });
                  },
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecoration(
                      border: Border.all(color: primary_11),
                    ),
                    child: Visibility(
                      visible: _isAccept,
                      child: const Icon(
                        Icons.done,
                        size: 12,
                      ),
                    ),
                  ),
                ),
                Container(
                    padding: const EdgeInsets.only(left: 5),
                    child: Row(
                      children: [
                        Text(
                          tr('sign_up.text_5'),
                          style: const TextStyle(
                              fontSize: 15,
                              color: showdow,
                              fontWeight: FontWeight.w400),
                        ),
                        GestureDetector(
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                isDismissible: false,
                                builder: (context) => _showTermsOfUse(""));
                          },
                          behavior: HitTestBehavior.opaque,
                          child: Text(
                            tr('sign_up.text_5_1'),
                            style: const TextStyle(
                                fontSize: 15,
                                color: primary_3,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ),
          const Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              setState(() {
                if (_isAccept) {
                  _isResult = false;
                  if (controllerInputFirstName?.text == null ||
                      controllerInputFirstName!.text.isEmpty) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_5');
                  } else if (controllerInputLastName?.text == null ||
                      controllerInputLastName!.text.isEmpty) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_6');
                  } else if (controllerInputEmail?.text == null ||
                      controllerInputEmail!.text.isEmpty) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_1');
                  } else if (controllerInputPassword?.text == null ||
                      controllerInputPassword!.text.isEmpty) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_2');
                  } else if (controllerInputPassword!.text.length < 8) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_9');
                  } else if (controllerInputPasswordConfirm?.text == null ||
                      controllerInputPasswordConfirm!.text.isEmpty) {
                    _isResult = true;
                    _strResult = tr('sign_up.error.err_4');
                  } else {
                    if (Utils().validateEmail(controllerInputEmail!.text) ==
                        false) {
                      _isResult = true;
                      _strResult = tr('error.no_email_format');
                    } else if (controllerInputPasswordConfirm?.text !=
                        controllerInputPassword?.text) {
                      _isResult = true;
                      _strResult = tr('sign_up.error.err_3');
                    } else {
                      //Call Apis
                      if (_isBTNEnable == true) {
                        _isBTNEnable = false;
                        context
                            .read<SignUpViewModel>()
                            .checkEmailDuplicate(controllerInputEmail?.text)
                            .then((value) {
                          if (value == true) {
                            //No existed
                            context
                                .read<SignUpViewModel>()
                                .customerRegisters(
                                  controllerInputFirstName?.text,
                                  controllerInputLastName?.text,
                                  controllerInputEmail?.text,
                                  controllerInputPassword!.text,
                                )
                                .then((value) {
                              if (value == true) {
                                _isBTNEnable = true;
                                context.router.push(AccountVerifyRoute());
                              } else {
                                setState(() {
                                  _isBTNEnable = true;
                                  _isResult = true;
                                  _strResult = tr('sign_up.error.err_8');
                                });
                              }
                            });
                          } else {
                            setState(() {
                              _isBTNEnable = true;
                              _isResult = true;
                              _strResult = tr('sign_up.error.err_7');
                            });
                          }
                        });
                      }
                    }
                  }
                }
              });
            },
            child: Align(
              alignment: Alignment.center,
              child: Container(
                padding:
                    EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  color: _isBTNEnable ? primary_3 : primary_3.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                      color:
                          _isBTNEnable ? primary_3 : primary_3.withOpacity(0)),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    tr('sign_up.text_6'),
                    style: const TextStyle(
                        fontSize: 15,
                        color: primary_5,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCreateAccount() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 45, top: 10),
        width: 100.w,
        child: Wrap(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                tr('sign_up.text_7'),
                style: TextStyle(color: textTerms, fontSize: 15),
              ),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const SignInRoute());
              },
              child: Text(
                tr('sign_up.text_8'),
                style: TextStyle(color: primary_10, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHiddenPass = !_isHiddenPass;
    });
  }

  void _togglePasswordConfirmView() {
    setState(() {
      _isHiddenPassConfirm = !_isHiddenPassConfirm;
    });
  }

  void _checkBTNEnable() {
    if (controllerInputFirstName!.text.isNotEmpty &&
        controllerInputLastName!.text.isNotEmpty &&
        controllerInputEmail!.text.isNotEmpty &&
        controllerInputPassword!.text.isNotEmpty &&
        controllerInputPasswordConfirm!.text.isNotEmpty &&
        controllerInputPassword?.text == controllerInputPasswordConfirm?.text &&
        _isAccept == true) {
      if (Utils().validateEmail(controllerInputEmail!.text)) {
        if (_isResult == true) {
          _isResult = false;
        }
        _isBTNEnable = true;
      } else {
        _isBTNEnable = false;
      }
    } else {
      _isBTNEnable = false;
    }
  }

  Widget _showTermsOfUse(String? htmlContent) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, left: 25, right: 25, bottom: 5),
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: SizedBox(
                    child: Icon(
                      Icons.info,
                      color: showdow,
                      size: 20,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5, top: 10),
                  child: SizedBox(
                    child: Text(
                      tr("sign_up.termsofuse.text_1"),
                      style: TextStyle(color: showdow, fontSize: 15),
                    ),
                  ),
                ),
                const Spacer(),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: const SizedBox(
                      child: Icon(
                        Icons.cancel,
                        color: showdow,
                        size: 20,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            width: 90.w,
            height: 1,
            color: primary_9,
          ),
          Container(
            margin: const EdgeInsets.only(top: 5, left: 25, right: 25),
            child: SingleChildScrollView(
              child: Html(
                data: htmlContent,
              ),
            ),
          )
        ],
      ),
    );
  }
}
