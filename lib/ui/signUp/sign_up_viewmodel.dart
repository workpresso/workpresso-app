import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/service/customer_service.dart';

@injectable
class SignUpViewModel extends ChangeNotifier {
  Future<void> setRememberPassword(String? password) async {
    final prefs = await SharedPreferences.getInstance();
    if (password != null) {
      if (password.isNotEmpty) {
        prefs.setString(rememberPassword, password);
      }
    }
  }

  Future<bool?> checkEmailDuplicate(String? email) async {
    return await getIt<CustomerService>().checkEmailDuplicate(email);
  }

  Future<bool?> customerRegisters(String? firstName, String? lastName,
      String? email, String? password) async {
    return await getIt<CustomerService>()
        .customerRegisters(firstName, lastName, email, password);
  }
}
