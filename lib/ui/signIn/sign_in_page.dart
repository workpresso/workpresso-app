import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/common_widgets/BaseAppBar.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/ui/signIn/sign_in_viewmodel.dart';
import 'package:workpresso/utils/utils.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  bool _isHidden = true;
  bool _isResult = false;
  bool _isRemember = false;
  String _strResult = "";
  bool _isShowPassword = false;
  TextEditingController? controllerInputEmail = TextEditingController();
  TextEditingController? controllerInputPassword = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          backgroundColor: primary_5,
          appBar: BaseAppBar(
            backgroupColor: primary_5,
            isShowAppBar: true,
            centerTitle: true,
            leftStatusBaseAppBar: LeftStatusBaseAppBar.none,
            onLeftPressCallback: () {
              Navigator.pop(context);
            },
            centerStatusBaseAppBar: CenterStatusBaseAppBar.none,
            titleBaseAppBar: tr('other.legal_requirements.policy'),
            rightStatusBaseAppBar: RightStatusBaseAppBar.none,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
                child: Column(
              children: [
                _buildTitle(),
                _buildInputSignIn(),
                _buildResultOutPut(),
                _buildButtonSignInAndRememPass(),
                Container(
                  width: 90.w,
                  height: 1,
                  color: primary_3,
                ),
                _buildCreateAccount(),
              ],
            )),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: EdgeInsets.only(top: 100, bottom: 30),
      width: 100.w,
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 25),
              child: Image.asset(
                AppAssets.logo,
                width: 207,
                height: 86,
              ),
            ),
            Text(
              tr('sign_in.title'),
              style: const TextStyle(
                  fontSize: 32, color: primary_3, fontWeight: FontWeight.w400),
            ),
            Text(
              tr('sign_in.des'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildResultOutPut() {
    return Visibility(
      visible: _isResult,
      child: Container(
        padding: const EdgeInsets.only(top: 15, left: 30),
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            const Icon(
              Icons.info,
              color: primary_12,
            ),
            Text(
              (_strResult),
              style: const TextStyle(
                  fontSize: 15, color: primary_12, fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInputSignIn() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 27),
      child: Column(
        children: [
          Container(
            width: 85.w,
            padding: EdgeInsets.only(left: 5),
            child: Text(
              tr('sign_in.text_1'),
              style: const TextStyle(
                  fontSize: 15, color: showdow, fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: EdgeInsets.all(5),
            // height: 15,
            child: TextField(
              controller: controllerInputEmail,
              keyboardType: TextInputType.emailAddress,
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(
                    fontSize: 14.0,
                    color: codeCountry,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Container(
            width: 90.w,
            height: 2.h,
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  tr('sign_in.text_2'),
                  style: const TextStyle(
                      fontSize: 15,
                      color: showdow,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Spacer(),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: GestureDetector(
                  onTap: () {
                    context.router.push(ForgotPasswordRoute());
                  },
                  child: Text(
                    tr('sign_in.text_3'),
                    style: const TextStyle(
                        fontSize: 15,
                        color: primary_10,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              )
            ],
          ),
          Container(
            width: 85.w,
            height: 40,
            color: primary_9,
            padding: EdgeInsets.all(5),
            // height: 15,
            child: Wrap(
              children: [
                Container(
                  width: 75.w,
                  height: 3.5.h,
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        if (value.isEmpty) {
                          _isShowPassword = false;
                        } else {
                          _isShowPassword = true;
                        }
                      });
                    },
                    obscureText: _isHidden,
                    controller: controllerInputPassword,
                    keyboardType: TextInputType.text,
                    inputFormatters: [LengthLimitingTextInputFormatter(20)],
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                          fontSize: 14.0,
                          color: codeCountry,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 3),
                  child: Visibility(
                    visible: _isShowPassword,
                    child: InkWell(
                      onTap: _togglePasswordView,
                      child: Icon(
                        _isHidden ? Icons.visibility : Icons.visibility_off,
                        color: primary_3,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtonSignInAndRememPass() {
    return Container(
      width: 100.w,
      margin: EdgeInsets.only(top: 18, bottom: 18, left: 35, right: 25),
      child: Row(
        children: [
          Visibility(
            visible: false,
            child: Container(
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        if (_isRemember) {
                          _isRemember = false;
                        } else {
                          _isRemember = true;
                        }
                      });
                    },
                    child: Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecoration(
                        border: Border.all(color: primary_11),
                      ),
                      child: Visibility(
                        visible: _isRemember,
                        child: const Icon(
                          Icons.done,
                          size: 12,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      tr("sign_in.text_4"),
                      style: const TextStyle(
                          fontSize: 15,
                          color: showdow,
                          fontWeight: FontWeight.w400),
                    ),
                  )
                ],
              ),
            ),
          ),
          Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              setState(() {
                _isResult = false;
                if (controllerInputEmail?.text == null ||
                    controllerInputEmail!.text.isEmpty) {
                  _isResult = true;
                  _strResult = tr('sign_in.error.err_1');
                } else if (controllerInputPassword?.text == null ||
                    controllerInputPassword!.text.isEmpty) {
                  _isResult = true;
                  _strResult = tr('sign_in.error.err_2');
                } else {
                  //Call Apis
                  if (Utils().validateEmail(controllerInputEmail!.text) ==
                      false) {
                    _isResult = true;
                    _strResult = tr('error.no_email_format');
                  } else {
                    if (_isRemember) {
                      context
                          .read<SignInViewModel>()
                          .setRememberPassword(controllerInputPassword!.text);
                    }
                    context
                        .read<SignInViewModel>()
                        .customerSignin(controllerInputEmail!.text,
                            controllerInputPassword!.text)
                        .then((value) {
                      if (value?.code == "not_found") {
                        setState(() {
                          _isResult = true;
                          _strResult = tr('sign_in.error.err_3');
                        });
                      } else {
                        context.read<SignInViewModel>().setTokenCustome(
                              value?.data?.token,
                              value?.data?.refreshToken,
                            );
                        context
                            .read<SignInViewModel>()
                            .setCustomerProfileNoRebuild()
                            .then((value) {
                          context.router.push(HomeRoute());
                        });
                      }
                    });
                  }
                }
              });
            },
            child: Align(
              alignment: Alignment.center,
              child: Container(
                padding:
                    EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  color: primary_3,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: primary_3),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    tr('sign_in.text_5'),
                    style: const TextStyle(
                        fontSize: 15,
                        color: primary_5,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCreateAccount() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 45, top: 10),
        width: 100.w,
        child: Wrap(
          children: [
            Text(
              tr('sign_in.text_6'),
              style: TextStyle(color: textTerms, fontSize: 15),
            ),
            GestureDetector(
              onTap: () {
                //Click
                context.router.push(const SignUpRoute());
              },
              child: Text(
                tr('sign_in.text_7'),
                style: TextStyle(color: primary_10, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}
