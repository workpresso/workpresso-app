import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/injection.dart';
import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/customer/reference_token.dart';
import 'package:workpresso/models/customer/verify_signin.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/customer_profile_service.dart';
import 'package:workpresso/service/customer_service.dart';

@injectable
class SignInViewModel extends ChangeNotifier {
  String? firstName;
  String? lastName;

  Future<void> setRememberPassword(String? password) async {
    final prefs = await SharedPreferences.getInstance();
    if (password != null) {
      if (password.isNotEmpty) {
        prefs.setString(rememberPassword, password);
      }
    }
  }

  Future<Resource<VerifySignin?>?> customerSignin(
      String? email, String? password) async {
    Resource<VerifySignin?>? result;
    result = await getIt<CustomerService>().customerSignin(email, password);
    return result;
  }

  Future<Resource<ReferenceToken?>?> customerRefreshToken() async {
    Resource<ReferenceToken?>? result;
    result = await getIt<CustomerService>().customerRefreshToken();
    return result;
  }

  Future<void> setTokenCustome(String? token, String? refreshTokenValue) async {
    final prefs = await SharedPreferences.getInstance();
    if (token != null && refreshTokenValue != null) {
      if (token.isNotEmpty && refreshTokenValue.isNotEmpty) {
        prefs.setString(tokenCustomer, token);
        prefs.setString(refreshToken, refreshTokenValue);
      }
    }
  }

  Future<void> setCustomerProfileNoRebuild() async {
    final prefs = await SharedPreferences.getInstance();
    Resource<CustomerProfile?>? result;
    result = await getIt<CustomerProfileService>().getProfileCustomer();
    if (result?.data?.customerId != null || result?.data?.customerId != "") {
      firstName = result?.data?.firstname ?? "";
      lastName = result?.data?.lastname ?? "";
      prefs.setString(customerId, result!.data!.customerId.toString());
    }
  }

  Future<bool?> customerForgot(String? email) async {
    return await getIt<CustomerService>().customerForgot(email);
  }
}
