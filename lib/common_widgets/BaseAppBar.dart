import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:workpresso/constants/app_assets.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_font.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  const BaseAppBar({
    this.onLeftPressCallback,
    this.onRightPressCallback,
    Key? key,
    this.titleBaseAppBar = "",
    this.isShowAppBar = false,
    this.centerTitle = true,
    this.leftStatusBaseAppBar = LeftStatusBaseAppBar.none,
    this.centerStatusBaseAppBar = CenterStatusBaseAppBar.none,
    this.rightStatusBaseAppBar = RightStatusBaseAppBar.none,
    this.rightStatusBaseAppBarVisibility,
    this.elevation = 0,
    this.bottom,
    String? title,
    this.backgroupColor = primary_5,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  final GestureTapCallback? onLeftPressCallback;
  final GestureTapCallback? onRightPressCallback;
  final String titleBaseAppBar;
  final bool isShowAppBar;
  final bool centerTitle;
  final LeftStatusBaseAppBar leftStatusBaseAppBar;
  final CenterStatusBaseAppBar centerStatusBaseAppBar;
  final RightStatusBaseAppBar rightStatusBaseAppBar;
  final bool? rightStatusBaseAppBarVisibility;
  final double elevation;
  final PreferredSizeWidget? bottom;
  final Color backgroupColor;

  @override
  final Size preferredSize;

  @override
  PreferredSizeWidget build(BuildContext context) {
    return _preferredSize();
  }

  PreferredSize _preferredSize() {
    return PreferredSize(
        preferredSize: const Size.fromHeight(64),
        child: Visibility(
          visible: isShowAppBar,
          child: _appBar(),
        ));
  }

  AppBar _appBar() {
    return AppBar(
      centerTitle: centerTitle,
      backgroundColor: backgroupColor,
      automaticallyImplyLeading: false,
      actions: iconRight(),
      title: _textTitle(),
      elevation: elevation,
      leading: leftStatusBaseAppBar != LeftStatusBaseAppBar.none
          ? _symbolLeft()
          : null,
      bottom: bottom,
    );
  }

  Widget _symbolLeft() {
    return Visibility(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
        child: Semantics(
            container: true,
            label: 'btnBack',
            child: IconButton(
              icon: _leftBaseAppBarAppearance(),
              onPressed: onLeftPressCallback,
            )),
      ),
      visible: leftStatusBaseAppBar != LeftStatusBaseAppBar.none,
    );
  }

  Widget _leftBaseAppBarAppearance() {
    if (leftStatusBaseAppBar == LeftStatusBaseAppBar.back) {
      return SvgPicture.asset(
        AppAssets.icon_back,
        color: primary_3,
      );
    } else {
      return const Icon(Icons.close, color: Colors.black);
    }
  }

  Widget _textTitle() {
    return Visibility(
      visible: titleBaseAppBar.isNotEmpty &&
          centerStatusBaseAppBar == CenterStatusBaseAppBar.title,
      child: Text(
        titleBaseAppBar,
        style: BaseTextStyle().bold(fontSize: 18, color: primary_3),
      ),
    );
  }

  List<Widget> iconRight() {
    return [_iconRight()];
  }

  Widget _iconRight() {
    if (rightStatusBaseAppBar == RightStatusBaseAppBar.profile) {
      return Visibility(
        visible: rightStatusBaseAppBarVisibility ?? true,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 6, 0),
          child: Semantics(
              container: true,
              label: 'btnClearCart',
              child: IconButton(
                icon: SvgPicture.asset(
                  AppAssets.icon_member_right,
                  color: primary_3,
                  width: 20,
                  height: 20,
                ),
                onPressed: onRightPressCallback,
              )),
        ),
      );
    } else {
      return Container();
    }
  }
}

enum LeftStatusBaseAppBar { none, back, close }

enum CenterStatusBaseAppBar {
  none,
  logo,
  title,
}

enum RightStatusBaseAppBar { none, close, profile }
