import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/interfaces/customer_profile_api.dart';
import 'package:workpresso/utils/api_manager.dart';

@singleton
class CustomerProfileService implements CustomerProfileApi {
  late Dio dio;
  late ApiManager apiManager;

  CustomerProfileService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<CustomerProfile?>>? getProfileCustomer() async {
    try {
      final result = await apiManager.getCustomerProfile();
      return Resource.success(data: CustomerProfile.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
