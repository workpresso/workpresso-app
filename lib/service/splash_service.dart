import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/splash/reference.dart';
import 'package:workpresso/models/splash/splash.dart';
import 'package:workpresso/service/interfaces/splash_api.dart';
import 'package:workpresso/utils/api_manager.dart';

@singleton
class SplashService implements SplashApi {
  late Dio dio;
  late ApiManager apiManager;

  SplashService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<Splash?>>? getSplashConfig() async {
    try {
      final result = await apiManager.getSplashConfig();
      return Resource.success(data: Splash.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<Reference?>>? getReference() async {
    try {
      final result = await apiManager.getReference();
      return Resource.success(data: Reference.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
