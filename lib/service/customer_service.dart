import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/customer/otp_request.dart';
import 'package:workpresso/models/customer/otp_verify.dart';
import 'package:workpresso/models/customer/reference_token.dart';
import 'package:workpresso/models/customer/register_customer.dart';
import 'package:workpresso/models/customer/upload_image_profile.dart';
import 'package:workpresso/models/customer/verify_signin.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/interfaces/customer_api.dart';
import 'package:workpresso/utils/api_manager.dart';

@singleton
class CustomerService implements CustomerApi {
  late Dio dio;
  late ApiManager apiManager;

  CustomerService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<OtpRequest?>>? otpRequest(String? phoneno) async {
    try {
      var body = {'phone_no': phoneno};
      final result = await apiManager.getCustomerOtpRequest(body);
      return Resource.success(data: OtpRequest.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<OtpVerify?>>? otpVerify(String? token, String? pin) async {
    try {
      var body = {'token': token, 'pin': pin};
      final result = await apiManager.getCustomerOTPVerify(body);
      return Resource.success(data: OtpVerify.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<VerifySignin?>>? verifySignin(
      String? phoneno, String? token, String? pin) async {
    try {
      var body = {'phone_no': phoneno, 'token': token, 'pin': pin};
      final result = await apiManager.getCustomerVerifySignin(body);
      return Resource.success(data: VerifySignin.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<RegisterCustomer?>>? registerCustomer(String? firstname,
      String? lastname, String? phoneNo, String? birthDay) async {
    try {
      var body = {
        'firstname': firstname,
        'lastname': lastname,
        'phone_no': phoneNo,
        'subscription': 'y',
        'birth_day': birthDay
      };
      final result = await apiManager.registerCustomer(body);
      return Resource.success(data: RegisterCustomer.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<CustomerProfile?>>? getCustomerProfile() async {
    try {
      final result = await apiManager.getCustomerProfile();
      return Resource.success(data: CustomerProfile.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<UploadImageProfile?>>? uploadImageProfile(
      String? partImage, String? nameImage) async {
    try {
      FormData formData = await FormData.fromMap({
        'file_upload':
            await MultipartFile.fromFile(partImage!, filename: nameImage),
      });
      final result = await dio.post(
          '${dotenv.env['API_UPLOAD_URL']}upload/v1/file',
          options: Options(
              headers: {HttpHeaders.contentTypeHeader: "multipart/form-data"}),
          data: formData);
      if (result.data['data'] != null) {
        return Resource.success(
            data: UploadImageProfile.fromJson(result.data['data']));
      } else {
        return Resource.error(code: "200", error: "Data null");
      }
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<void>>? updateProfile(String? firstName, String? lastName,
      String? image, String? birthDay, String? phoneNo) async {
    try {
      var body = {
        'firstname': firstName,
        'lastname': lastName,
        'image': image,
        'phone_no': phoneNo,
        'birth_day': birthDay
      };
      final result = await apiManager.updateProfile(body);
      return Resource.success(data: result);
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  //New
  @override
  Future<Resource<VerifySignin?>>? customerSignin(
      String? email, String? password) async {
    try {
      var body = {'email': email, 'password': password};
      final result = await apiManager.getCustomerVerifySignin(body);
      return Resource.success(data: VerifySignin.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<ReferenceToken?>>? customerRefreshToken() async {
    try {
      String? refToken;
      final prefs = await SharedPreferences.getInstance();
      refToken = prefs.getString(refreshToken);
      dio.options.headers['Token'] = refToken;
      apiManager = ApiManager(dio);
      final result = await apiManager.customerRefresh();
      return Resource.success(data: ReferenceToken.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<bool>? customerForgot(String? email) async {
    try {
      var body = {'email': email};
      final result = await apiManager.customerForgot(body);
      if (result.data) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      if (e is DioError) {
        return false;
      }
      return false;
    }
  }

  @override
  Future<bool>? checkEmailDuplicate(String? email) async {
    try {
      var body = {'email': email};
      final result = await apiManager.CheckEmailDuplicate(body);
      if (result.code == "is_existed") {
        return false;
      } else {
        return true;
      }
    } catch (e) {
      if (e is DioError) {
        return false;
      }
      return false;
    }
  }

  @override
  Future<bool>? customerRegisters(String? firstName, String? lastName,
      String? email, String? password) async {
    try {
      var body = {
        'firstname': firstName,
        'lastname': lastName,
        'email': email,
        'password': password
      };
      final result = await apiManager.customerRegisters(body);
      if (result.data == true) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      if (e is DioError) {
        return false;
      }
      return false;
    }
  }
}
