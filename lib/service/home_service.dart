import 'package:dio/dio.dart';
import 'package:workpresso/models/banner.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/service/interfaces/home_api.dart';
import 'package:workpresso/utils/api_manager.dart';
import 'package:injectable/injectable.dart';

@singleton
class HomeService implements HomeApi {
  late Dio dio;
  late ApiManager apiManager;

  HomeService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<Banner?>>? getBannerTop() async {
    try {
      final result = await apiManager.getBannerTop();
      return Resource.success(data: Banner.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<Banner?>>? getBannerSlide() async {
    try {
      final result = await apiManager.getBannerSlide();
      return Resource.success(data: Banner.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<Banner?>>? getBannerPromotion() async {
    try {
      final result = await apiManager.getBannerPromotion();
      return Resource.success(data: Banner.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<Banner?>>? getBannerIcon() async {
    try {
      final result = await apiManager.getBannerIcon();
      return Resource.success(data: Banner.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
