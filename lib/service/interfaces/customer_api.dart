import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/customer/otp_request.dart';
import 'package:workpresso/models/customer/otp_verify.dart';
import 'package:workpresso/models/customer/reference_token.dart';
import 'package:workpresso/models/customer/register_customer.dart';
import 'package:workpresso/models/customer/upload_image_profile.dart';
import 'package:workpresso/models/customer/verify_signin.dart';
import 'package:workpresso/models/resource.dart';

abstract class CustomerApi {
  Future<Resource<OtpRequest?>>? otpRequest(String? phoneno);
  Future<Resource<VerifySignin?>>? verifySignin(
      String? phoneno, String? token, String? pin);
  Future<Resource<OtpVerify?>>? otpVerify(String? token, String? pin);
  Future<Resource<RegisterCustomer?>>? registerCustomer(
      String? firstname, String? lastname, String? phoneNo, String? birthDay);
  Future<Resource<CustomerProfile?>>? getCustomerProfile();
  Future<Resource<UploadImageProfile?>>? uploadImageProfile(
      String? partImage, String? nameImage);
  Future<Resource<void>>? updateProfile(String? firstName, String? lastName,
      String? image, String? birthDay, String? phoneNo);
  //New
  Future<Resource<VerifySignin?>>? customerSignin(
      String? email, String? password);
  Future<Resource<ReferenceToken?>>? customerRefreshToken();
  Future<bool>? customerForgot(String? email);
  Future<bool>? checkEmailDuplicate(String? email);
  Future<bool>? customerRegisters(
      String? firstName, String? lastName, String? email, String? password);
}
