import 'package:workpresso/models/banner.dart';
import 'package:workpresso/models/resource.dart';

abstract class HomeApi {
  Future<Resource<Banner?>>? getBannerTop();
  Future<Resource<Banner?>>? getBannerSlide();
  Future<Resource<Banner?>>? getBannerPromotion();
  Future<Resource<Banner?>>? getBannerIcon();
}
