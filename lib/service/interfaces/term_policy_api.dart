import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/term_policy/policy.dart';
import 'package:workpresso/models/term_policy/term.dart';

abstract class TermPolicyApi {
  Future<Resource<TermData?>>? getTerm();
  Future<Resource<PolicyData?>>? getPolicy();
}
