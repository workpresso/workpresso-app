import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/splash/reference.dart';
import 'package:workpresso/models/splash/splash.dart';

abstract class SplashApi {
  Future<Resource<Splash?>>? getSplashConfig();
  Future<Resource<Reference?>>? getReference();
}
