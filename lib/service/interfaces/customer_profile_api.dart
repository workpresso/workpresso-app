import 'package:workpresso/models/customer/customer_profile.dart';
import 'package:workpresso/models/resource.dart';

abstract class CustomerProfileApi {
  Future<Resource<CustomerProfile?>>? getProfileCustomer();
}
