import 'package:dio/dio.dart';
import 'package:workpresso/models/resource.dart';
import 'package:workpresso/models/term_policy/policy.dart';
import 'package:workpresso/models/term_policy/term.dart';
import 'package:workpresso/service/interfaces/term_policy_api.dart';
import 'package:workpresso/utils/api_manager.dart';
import 'package:injectable/injectable.dart';

@singleton
class TermPolicyService implements TermPolicyApi {
  late Dio dio;
  late ApiManager apiManager;

  TermPolicyService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<TermData?>>? getTerm() async {
    try {
      final result = await apiManager.getTerm();
      return Resource.success(data: TermData.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }

  @override
  Future<Resource<PolicyData?>>? getPolicy() async {
    try {
      final result = await apiManager.getPolicy();
      return Resource.success(data: PolicyData.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
