import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:workpresso/constants/app_colors.dart';
import 'package:workpresso/constants/app_key.dart';
import 'package:workpresso/environment_config.dart';
import 'package:workpresso/translations/generated/codegen_loader.g.dart';
import 'package:workpresso/ui/coupon/coupon_viewmodel.dart';
import 'package:workpresso/ui/forgotPassword/forgot_password_send_viewmodel.dart';
import 'package:workpresso/ui/forgotPassword/forgot_password_viewmodel.dart';
import 'package:workpresso/ui/home/home_viewmodel.dart';
import 'package:workpresso/ui/notify/notify_viewmodel.dart';
import 'package:workpresso/ui/other/other_viewmodel.dart';
import 'package:workpresso/ui/policy/policy_viewmodel.dart';
import 'package:workpresso/ui/profile/profile_viewmodel.dart';
import 'package:workpresso/ui/registers/step_four/step_four_viewmodel.dart';
import 'package:workpresso/ui/registers/step_one/step_one_viewmodel.dart';
import 'package:workpresso/ui/registers/step_tree/step_tree_viewmodel.dart';
import 'package:workpresso/ui/registers/step_two/step_two_viewmodel.dart';
import 'package:workpresso/ui/route/router.gr.dart';
import 'package:workpresso/ui/selectLanguage/select_language_viewmodel.dart';
import 'package:workpresso/ui/signIn/sign_in_viewmodel.dart';
import 'package:workpresso/ui/signUp/sign_up_viewmodel.dart';
import 'package:workpresso/ui/splash/splash_screen_viewmodel.dart';
import 'package:workpresso/ui/term/term_viewmodel.dart';
import 'package:workpresso/ui/tutorial/tutorial_viewmodel.dart';
import 'package:workpresso/utils/localization_util.dart';
import 'package:workpresso/utils/utils.dart';

import 'injection.dart';

Future<void> mainApp() async {
  await dotenv.load(fileName: EnvironmentConfig.fileName);
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  Intl.defaultLocale = 'th';
  configureInjection(EnvironmentConfig.env);
  checkDeviceId();
  LocalizationUtil();
  runApp(EasyLocalization(
      path: 'lib/translations/',
      useOnlyLangCode: true,
      startLocale: const Locale('th'),
      supportedLocales: const [Locale('th'), Locale('en')],
      assetLoader: const CodegenLoader(),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => getIt<SplashScreenViewModel>()),
          ChangeNotifierProvider(
              create: (_) => getIt<SelectLanguageViewmodel>()),
          ChangeNotifierProvider(create: (_) => getIt<HomeViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<CouponViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<ProfileViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<NotifyViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<OtherViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<TermViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<PolicyViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<StepOnePageViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<StepTwoPageViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<StepTreePageViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<StepFourPageViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<SignInViewModel>()),
          ChangeNotifierProvider(
              create: (_) => getIt<ForgotPasswordViewModel>()),
          ChangeNotifierProvider(
              create: (_) => getIt<ForgotPasswordSendViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<SignUpViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<TutorialViewModel>()),
        ],
        child: MyApp(),
      )));
}

Future<void> checkDeviceId() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getString(deviceId) == null) {
    prefs.setString(deviceId, Utils().generateDeviceId());
  }
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final AppRouter _appRouter = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp.router(
        theme: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: primary_1,
            colorScheme: Theme.of(context).colorScheme.copyWith(
                  primary: primary_1,
                ),
            textSelectionTheme:
                const TextSelectionThemeData(cursorColor: primary_1),
            textTheme: const TextTheme(
              overline: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              caption: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              button: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              bodyText1: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              bodyText2: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline1: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline2: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline3: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline4: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline5: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              headline6: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              subtitle1: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
              subtitle2: TextStyle(
                fontFamily: 'Prompt',
                fontFamilyFallback: ['Prompt'],
              ),
            )),
        debugShowCheckedModeBanner: false,
        routerDelegate: _appRouter.delegate(),
        routeInformationParser: _appRouter.defaultRouteParser(),
        locale: context.locale,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        builder: EasyLoading.init(),
      );
    });
  }
}
