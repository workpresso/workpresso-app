import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:workpresso/base/base.dart';
import 'package:workpresso/environment_config.dart';
import 'package:workpresso/models/core/response_generic_data.dart';

part 'api_manager.g.dart';

class AppInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      print('onError : ${err.message}');
    }
    super.onError(err, handler);
  }
}

@RestApi()
abstract class ApiManager {
  factory ApiManager(Dio dio) {
    dio.options = BaseOptions(
      baseUrl: EnvironmentConfig.apiUrl,
      receiveTimeout: ApisConfig.receiveTimeout,
      connectTimeout: ApisConfig.connectTimeout,
      contentType: ApisConfig.jsonContentType,
    );
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      Base().getTokenHeader().then((value) {
        options.headers['Authorization'] = ApisConfig.authorizationApis;
        options.headers['Language'] = 1;
        if (value != null) {
          options.headers['Token'] = value;
        }
        // Base().getUploadFileHeader().then((value) async {
        //   if (value != null) {
        //     if (value == true) {
        //       final prefs = await SharedPreferences.getInstance();
        //       prefs.setBool(isUploadFile, false);
        //       options.headers['Authorization'] = ApisConfig.authorizationApis;
        //     }
        //   }
        // });
        handler.next(options);
      });

      Base().getReference().then((value) {
        if (value != null) {
          options.headers['reference-id'] = value;
        }
        handler.next(options);
      });
    }));
    if (kDebugMode) {
      dio.interceptors.add(AppInterceptor());
      dio.interceptors
          .add(LogInterceptor(requestBody: true, responseBody: true));
    }

    return _ApiManager(dio);
  }

  @GET(Apis.kSplashConfig)
  Future<ResponseGenericData> getSplashConfig();

  @GET(Apis.kReference)
  Future<ResponseGenericData> getReference();

  @POST(Apis.kCustomerOtpRequest)
  Future<ResponseGenericData> getCustomerOtpRequest(
      @Body() Map<String, dynamic> body);

  @POST(Apis.kCustomerOtpVerify)
  Future<ResponseGenericData> getCustomerOTPVerify(
      @Body() Map<String, dynamic> body);

  @POST(Apis.kCustomerSignin)
  Future<ResponseGenericData> getCustomerVerifySignin(
      @Body() Map<String, dynamic> body);

  @POST(Apis.kCustomerRegister)
  Future<ResponseGenericData> registerCustomer(
      @Body() Map<String, dynamic> body);

  @GET(Apis.kCustomerProfile)
  Future<ResponseGenericData> getCustomerProfile();

  @GET(Apis.kTerm)
  Future<ResponseGenericData> getTerm();

  @GET(Apis.kPolicy)
  Future<ResponseGenericData> getPolicy();

  @GET(Apis.kHomeBannerTop)
  Future<ResponseGenericData> getBannerTop();

  @GET(Apis.kHomeBannerSlide)
  Future<ResponseGenericData> getBannerSlide();

  @GET(Apis.kHomeBannerPromotion)
  Future<ResponseGenericData> getBannerPromotion();

  @GET(Apis.kHomeBannerIcon)
  Future<ResponseGenericData> getBannerIcon();

  @POST(Apis.kUploadImageProfile)
  Future<ResponseGenericData> uploadImageProfile(
      @Body() Map<String, dynamic> body);

  @PATCH(Apis.kCustomerProfileUpdate)
  Future<ResponseGenericData> updateProfile(@Body() Map<String, dynamic> body);

  @POST(Apis.kCustomerRefresh)
  Future<ResponseGenericData> customerRefresh();

  @POST(Apis.kCustomerForgot)
  Future<ResponseGenericData> customerForgot(@Body() Map<String, dynamic> body);

  @POST(Apis.kCheckEmailDuplicate)
  Future<ResponseGenericData> CheckEmailDuplicate(
      @Body() Map<String, dynamic> body);

  @POST(Apis.kCustomerRegisters)
  Future<ResponseGenericData> customerRegisters(
      @Body() Map<String, dynamic> body);
}

class Apis {
  static const String kSplashConfig =
      '/v3/ce356e94-2d5c-4dc6-9ed0-caf108e45e2d';
  static const String kHomeBannerTop = 'cms/v1/banner/top';
  static const String kHomeBannerSlide = 'cms/v1/banner/slide';
  static const String kHomeBannerPromotion = 'cms/v1/banner/promotion';
  static const String kHomeBannerIcon = 'cms/v1/banner/icon';
  static const String kCustomerOtpRequest = 'customer/v1/otp/request';
  static const String kCustomerOtpVerify = 'customer/v1/otp/verify';
  static const String kCustomerSignin = 'customer/v1/signin';
  static const String kCustomerRegister = 'customer/v1/profile';
  static const String kCustomerProfile = 'customer/v1/customers';
  static const String kCustomerProfileUpdate = 'customer/v1/profile';
  static const String kTerm = 'cms/v1/page/2';
  static const String kPolicy = 'cms/v1/page/1';
  static const String kUploadImageProfile = 'upload/v1/file';
  static const String kReference = '/common/v1/reference_id';
  static const String kCustomerRefresh = '/customer/v1/refresh';
  static const String kCustomerForgot = '/customer/v1/customers/forgot';

  static const String kCheckEmailDuplicate = '/customer/v1/email';
  static const String kCustomerRegisters = '/customer/v1/register';
}

class ApisConfig {
  static const String jsonContentType = 'application/json';
  static const int receiveTimeout = 30 * 1000; //30000 ms = 30 sec
  static const int connectTimeout = 30 * 1000; //30000 ms = 30 sec
  static const String authorizationApis = "Z2dnOmF4eERKd1ZqbDM=";
}
