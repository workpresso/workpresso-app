import 'package:workpresso/app_config.dart';
import 'package:workpresso/environment_config.dart';
import 'package:workpresso/main.dart';

Future<void> main() async {
  AppConfig devAppConfig = AppConfig(flavorName: Environments.prod);
  EnvironmentConfig.getInstance(flavorName: devAppConfig.flavorName);
  mainApp();
}
