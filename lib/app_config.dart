class AppConfig {
  final String flavorName;

  AppConfig({required this.flavorName});

  static AppConfig? _instance;

  static AppConfig getInstance({flavorName}) {
    if (_instance == null) {
      _instance = AppConfig(flavorName: flavorName);
      return _instance!;
    }
    return _instance!;
  }
}
