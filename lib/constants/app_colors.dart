import 'package:flutter/material.dart';

//Main
const Color primary_1 = Color(0xfffffbee);
const Color primary_2 = Color(0xffFCF1CC);
const Color primary_3 = Color(0xff9A7F63);
const Color primary_4 = Color(0xffD9D9D9);
const Color primary_5 = Color(0xffffffff);
const Color primary_6 = Color(0xffFFFBEE);
const Color showdow = Color(0xff000000);
const Color textdes = Color(0xff626262);
const Color indicatorOpa = Color.fromRGBO(58, 54, 53, 0.7);
const Color codeCountry = Color(0xffCDCDCD);
const Color textTerms = Color(0xff9F9C9C);
const Color primary_7 = Color(0xffF5F5F5);
const Color primary_8 = Color(0xffF5D159);
const Color primary_9 = Color(0xffE5E4E1);
const Color primary_10 = Color(0xff1BAAB8);
const Color primary_11 = Color(0xffCCD2E3);
const Color primary_12 = Color(0xffED1C24);
