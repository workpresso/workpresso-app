class AppAssets {
  static const String logo = 'assets/images/logo.png';
  static const String flag_th = 'assets/images/flag/thai.png';
  static const String flag_en = 'assets/images/flag/eng.png';
  static const String banner_ex = 'assets/images/bg_ex.png';
  static const String icon_next = 'assets/icon/next_icon.svg';
  static const String icon_back = 'assets/icon/icon-back.svg';
  static const String icon_member_right = 'assets/icon/member_right_icon.svg';
  static const String icon_member = 'assets/icon/member_icon.svg';
  static const String proimage_1 = 'assets/images/pro_1.png';
  static const String proimage_2 = 'assets/images/pro_2.png';
  static const String icon_logout = 'assets/icon/icon_logout.png';

// tutorial
  static const String tutorial_1 = 'assets/images/tutorial/tutorial_1.png';
  static const String tutorial_2 = 'assets/images/tutorial/tutorial_2.png';
  static const String tutorial_3 = 'assets/images/tutorial/tutorial_3.png';
  static const String tutorial_4 = 'assets/images/tutorial/tutorial_4.png';
  static const String tutorial_5 = 'assets/images/tutorial/tutorial_5.png';
  static const String forgot_pass_1 = 'assets/images/email_sented.png';
  static const String arrow_left_while = 'assets/images/arrowLeftWhile.png';
  static const String arrow_rigth_while = 'assets/images/arrow_head_right.png';

//  Menu
  static const String menuHome = 'assets/icon/home.svg';
  static const String menuCoupon = 'assets/icon/coupon.svg';
  static const String menuProfile = 'assets/icon/profile_icon.png';
  static const String menuNotify = 'assets/icon/notify_icon.svg';
  static const String menuOther = 'assets/icon/other_icon.svg';
  static const String menuCouponCS = 'assets/icon/icon_comeing_coupon.svg';
  static const String menuNotifyCS = 'assets/icon/icon_comeing_noti.svg';
  static const String imagePlaceHolde = 'assets/images/placeHolder.png';

//  Other
  static const String other_line = 'assets/icon/other/line.png';
  static const String other_facebook = 'assets/icon/other/facebook.png';
  static const String other_youtube = 'assets/icon/other/youtube.png';
  static const String other_line_hr = 'assets/icon/other/lineYellow.png';
  static const String other_email = 'assets/icon/other/email.png';
  static const String other_call = 'assets/icon/other/call.png';
  static const String other_noti = 'assets/icon/other/noti.png';
  static const String other_condition = 'assets/icon/other/condition.png';
  static const String other_policy = 'assets/icon/other/policy.png';

  static const String icon_success = 'assets/icon/icon_success.png';
  static const String icon_verify = 'assets/images/verify-icon.png';
}
