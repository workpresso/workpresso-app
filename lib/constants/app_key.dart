// Key SharedPreferences
const String firebaseToken = "firebase_token";
const String deviceId = "device_id";
const String firstInstall = "first_install";
const String isSignIn = "is_sign_in";
const String tokenCustomer = "tokenCustomer";
const String isUploadFile = "isUploadFile";
const String rememberPassword = "rememberPassword";
const String referenceId = "referenceId";
const String refreshToken = "refreshToken";
const String customerId = "customerId";
