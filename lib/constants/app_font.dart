import 'package:flutter/material.dart';

class BaseTextStyle {
  TextStyle regular(
      {double fontSize = 16,
      Color color = Colors.black,
      FontWeight fontWeight = FontWeight.normal}) {
    return TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        fontFamily: 'Prompt',
        fontFamilyFallback: const ['Prompt']);
  }

  TextStyle bold(
      {double fontSize = 16,
      Color color = Colors.black,
      FontWeight fontWeight = FontWeight.bold}) {
    return TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        fontFamily: 'Prompt',
        fontFamilyFallback: const ['Prompt']);
  }

  /// Custom your style no need to set font family
  TextStyle custom(
      {double? fontSize,
      Color? color,
      FontWeight? fontWeight,
      TextDecoration? decoration,
      FontStyle? fontStyle,
      double? height,
      Color? decorationColor,
      TextDecorationStyle? decorationStyle}) {
    return TextStyle(
        fontSize: fontSize,
        color: color,
        decoration: decoration,
        fontWeight: fontWeight,
        fontStyle: fontStyle,
        decorationColor: decorationColor,
        decorationStyle: decorationStyle,
        height: height,
        fontFamily: 'Prompt',
        fontFamilyFallback: const ['Prompt']);
  }
}
