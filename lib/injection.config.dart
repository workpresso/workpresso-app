// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'service/customer_profile_service.dart' as _i4;
import 'service/customer_service.dart' as _i5;
import 'service/home_service.dart' as _i8;
import 'service/splash_service.dart' as _i19;
import 'service/term_policy_service.dart' as _i24;
import 'ui/coupon/coupon_viewmodel.dart' as _i3;
import 'ui/forgotPassword/forgot_password_send_viewmodel.dart' as _i6;
import 'ui/forgotPassword/forgot_password_viewmodel.dart' as _i7;
import 'ui/home/home_viewmodel.dart' as _i9;
import 'ui/notify/notify_viewmodel.dart' as _i11;
import 'ui/other/other_viewmodel.dart' as _i12;
import 'ui/policy/policy_viewmodel.dart' as _i13;
import 'ui/profile/profile_viewmodel.dart' as _i14;
import 'ui/registers/step_four/step_four_viewmodel.dart' as _i20;
import 'ui/registers/step_one/step_one_viewmodel.dart' as _i21;
import 'ui/registers/step_tree/step_tree_viewmodel.dart' as _i22;
import 'ui/registers/step_two/step_two_viewmodel.dart' as _i23;
import 'ui/selectLanguage/select_language_viewmodel.dart' as _i15;
import 'ui/signIn/sign_in_viewmodel.dart' as _i16;
import 'ui/signUp/sign_up_viewmodel.dart' as _i17;
import 'ui/splash/splash_screen_viewmodel.dart' as _i18;
import 'ui/term/term_viewmodel.dart' as _i25;
import 'ui/tutorial/tutorial_viewmodel.dart' as _i26;
import 'utils/localization_util.dart'
    as _i10; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  gh.factory<_i3.CouponViewModel>(() => _i3.CouponViewModel());
  gh.singleton<_i4.CustomerProfileService>(_i4.CustomerProfileService());
  gh.singleton<_i5.CustomerService>(_i5.CustomerService());
  gh.factory<_i6.ForgotPasswordSendViewModel>(
      () => _i6.ForgotPasswordSendViewModel());
  gh.factory<_i7.ForgotPasswordViewModel>(() => _i7.ForgotPasswordViewModel());
  gh.singleton<_i8.HomeService>(_i8.HomeService());
  gh.factory<_i9.HomeViewModel>(() => _i9.HomeViewModel());
  gh.factory<_i10.LocalizationUtil>(() => _i10.LocalizationUtil());
  gh.factory<_i11.NotifyViewModel>(() => _i11.NotifyViewModel());
  gh.factory<_i12.OtherViewModel>(() => _i12.OtherViewModel());
  gh.factory<_i13.PolicyViewModel>(() => _i13.PolicyViewModel());
  gh.factory<_i14.ProfileViewModel>(() => _i14.ProfileViewModel());
  gh.factory<_i15.SelectLanguageViewmodel>(
      () => _i15.SelectLanguageViewmodel());
  gh.factory<_i16.SignInViewModel>(() => _i16.SignInViewModel());
  gh.factory<_i17.SignUpViewModel>(() => _i17.SignUpViewModel());
  gh.factory<_i18.SplashScreenViewModel>(() => _i18.SplashScreenViewModel());
  gh.singleton<_i19.SplashService>(_i19.SplashService());
  gh.factory<_i20.StepFourPageViewModel>(() => _i20.StepFourPageViewModel());
  gh.factory<_i21.StepOnePageViewModel>(() => _i21.StepOnePageViewModel());
  gh.factory<_i22.StepTreePageViewModel>(() => _i22.StepTreePageViewModel());
  gh.factory<_i23.StepTwoPageViewModel>(() => _i23.StepTwoPageViewModel());
  gh.singleton<_i24.TermPolicyService>(_i24.TermPolicyService());
  gh.factory<_i25.TermViewModel>(() => _i25.TermViewModel());
  gh.factory<_i26.TutorialViewModel>(() => _i26.TutorialViewModel());
  return get;
}
