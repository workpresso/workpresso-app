import 'package:json_annotation/json_annotation.dart';

part 'term.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class TermData {
  String? detail;
  String? title;
  int? id;

  TermData({
    this.detail,
    this.title,
    this.id,
  });
  factory TermData.fromJson(Map<String, dynamic> json) =>
      _$TermDataFromJson(json);
}
