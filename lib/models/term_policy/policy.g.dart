// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'policy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PolicyData _$PolicyDataFromJson(Map<String, dynamic> json) => PolicyData(
      detail: json['detail'] as String?,
      title: json['title'] as String?,
      id: json['id'] as int?,
    );
