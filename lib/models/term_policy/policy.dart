import 'package:json_annotation/json_annotation.dart';

part 'policy.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class PolicyData {
  String? detail;
  String? title;
  int? id;

  PolicyData({
    this.detail,
    this.title,
    this.id,
  });
  factory PolicyData.fromJson(Map<String, dynamic> json) =>
      _$PolicyDataFromJson(json);
}
