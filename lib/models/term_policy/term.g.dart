// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'term.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TermData _$TermDataFromJson(Map<String, dynamic> json) => TermData(
      detail: json['detail'] as String?,
      title: json['title'] as String?,
      id: json['id'] as int?,
    );
