import 'package:json_annotation/json_annotation.dart';

part 'reference.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Reference {
  String? referenceId;
  // List<dynamic>? data;
  Reference({
    this.referenceId,
    // this.data,
  });
  factory Reference.fromJson(Map<String, dynamic> json) =>
      _$ReferenceFromJson(json);
}
