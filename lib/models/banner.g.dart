// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Banner _$BannerFromJson(Map<String, dynamic> json) => Banner(
      banners: (json['banners'] as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Bannerlists.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Bannerlists _$BannerlistsFromJson(Map<String, dynamic> json) => Bannerlists(
      id: json['id'] as int?,
      position: json['position'] as String?,
      publishOn: json['publish_on'] as String?,
      thumbnail: json['thumbnail'] == null
          ? null
          : ThumbnailData.fromJson(json['thumbnail'] as Map<String, dynamic>),
      title: json['title'] as String?,
    );

ThumbnailData _$ThumbnailDataFromJson(Map<String, dynamic> json) =>
    ThumbnailData(
      cover: json['cover'] as String?,
      original: json['original'] as String?,
      thumbnail: json['thumbnail'] as String?,
    );
