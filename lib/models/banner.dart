import 'package:json_annotation/json_annotation.dart';

part 'banner.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Banner {
  List<Bannerlists?>? banners;

  Banner({
    this.banners,
  });
  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Bannerlists {
  int? id;
  String? position;
  String? publishOn;
  ThumbnailData? thumbnail;
  String? title;

  Bannerlists({
    this.id,
    this.position,
    this.publishOn,
    this.thumbnail,
    this.title,
  });
  factory Bannerlists.fromJson(Map<String, dynamic> json) =>
      _$BannerlistsFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class ThumbnailData {
  String? cover;
  String? original;
  String? thumbnail;

  ThumbnailData({
    this.cover,
    this.original,
    this.thumbnail,
  });
  factory ThumbnailData.fromJson(Map<String, dynamic> json) =>
      _$ThumbnailDataFromJson(json);
}
