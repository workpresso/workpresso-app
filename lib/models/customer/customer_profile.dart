import 'package:json_annotation/json_annotation.dart';

part 'customer_profile.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class CustomerProfile {
  int? customerId;
  String? firstname;
  String? lastname;
  String? phoneNo;

  CustomerProfile({
    this.customerId,
    this.firstname,
    this.lastname,
    this.phoneNo,
  });
  factory CustomerProfile.fromJson(Map<String, dynamic> json) =>
      _$CustomerProfileFromJson(json);
}
