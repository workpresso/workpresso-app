// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerProfile _$CustomerProfileFromJson(Map<String, dynamic> json) =>
    CustomerProfile(
      customerId: json['customer_id'] as int?,
      firstname: json['firstname'] as String?,
      lastname: json['lastname'] as String?,
      phoneNo: json['phone_no'] as String?,
    );
