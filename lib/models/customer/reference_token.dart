import 'package:json_annotation/json_annotation.dart';

part 'reference_token.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class ReferenceToken {
  String? refreshToken;
  String? token;

  ReferenceToken({
    this.refreshToken,
    this.token,
  });
  factory ReferenceToken.fromJson(Map<String, dynamic> json) =>
      _$ReferenceTokenFromJson(json);
}
