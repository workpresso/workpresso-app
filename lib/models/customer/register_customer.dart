import 'package:json_annotation/json_annotation.dart';

part 'register_customer.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class RegisterCustomer {
  String? token;

  RegisterCustomer({
    this.token,
  });
  factory RegisterCustomer.fromJson(Map<String, dynamic> json) =>
      _$RegisterCustomerFromJson(json);
}
