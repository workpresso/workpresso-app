import 'package:json_annotation/json_annotation.dart';

part 'upload_image_profile.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class UploadImageProfile {
  String? fileUrl;
  String? filePath;
  String? fileName;
  int? fileSize;

  UploadImageProfile({
    this.fileUrl,
    this.filePath,
    this.fileName,
    this.fileSize,
  });
  factory UploadImageProfile.fromJson(Map<String, dynamic> json) =>
      _$UploadImageProfileFromJson(json);
}
