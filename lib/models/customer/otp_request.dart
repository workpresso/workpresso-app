import 'package:json_annotation/json_annotation.dart';

part 'otp_request.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class OtpRequest {
  String? refno;
  String? status;
  String? token;
  String? typeCustomer;

  OtpRequest({
    this.refno,
    this.status,
    this.token,
    this.typeCustomer,
  });
  factory OtpRequest.fromJson(Map<String, dynamic> json) =>
      _$OtpRequestFromJson(json);
}
