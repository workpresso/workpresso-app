// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_image_profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadImageProfile _$UploadImageProfileFromJson(Map<String, dynamic> json) =>
    UploadImageProfile(
      fileUrl: json['file_url'] as String?,
      filePath: json['file_path'] as String?,
      fileName: json['file_name'] as String?,
      fileSize: json['file_size'] as int?,
    );
