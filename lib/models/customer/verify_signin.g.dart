// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_signin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifySignin _$VerifySigninFromJson(Map<String, dynamic> json) => VerifySignin(
      token: json['token'] as String?,
      refreshToken: json['refresh_token'] as String?,
    );
