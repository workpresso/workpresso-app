import 'package:json_annotation/json_annotation.dart';

part 'verify_signin.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class VerifySignin {
  String? token;
  String? refreshToken;

  VerifySignin({
    this.token,
    this.refreshToken,
  });
  factory VerifySignin.fromJson(Map<String, dynamic> json) =>
      _$VerifySigninFromJson(json);
}
