// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reference_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReferenceToken _$ReferenceTokenFromJson(Map<String, dynamic> json) =>
    ReferenceToken(
      refreshToken: json['refresh_token'] as String?,
      token: json['token'] as String?,
    );
