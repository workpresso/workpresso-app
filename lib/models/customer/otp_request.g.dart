// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OtpRequest _$OtpRequestFromJson(Map<String, dynamic> json) => OtpRequest(
      refno: json['refno'] as String?,
      status: json['status'] as String?,
      token: json['token'] as String?,
      typeCustomer: json['type_customer'] as String?,
    );
